# 참고 사이트 https://www.nextree.io/module-federation/

## 구성 폴더
### 리모트 : module_federation_remote (react)
### 호스트 : module_federation_host (nextjs)

## 주의 사항

nextjs 13 버전부터 app 디렉토리 지원, app 디렉토리로 하는 경우 "Error: Shared module react-dom doesn't exist in shared scope default" 발생
<br/>
그래서 구버전(12) 방식으로 pages 폴더를 만들고 index.jsx에 리모트의 컴포넌트를 삽입

리모트: webpack.config.js
<br/>
호스트: next.config.js
