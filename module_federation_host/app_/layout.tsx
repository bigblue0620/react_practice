import React from "react";

export default function RootLayout({
    children,
  }: {
    children: React.ReactNode;
  }) {
    return (
      <html lang="ko">
        {/* <script src="http://localhost:8080/remoteEntry.js" /> */}
        <body>{children}</body>
      </html>
    );
}