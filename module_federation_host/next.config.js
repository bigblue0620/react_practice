const { NextFederationPlugin } = require('@module-federation/nextjs-mf');

const nextConfig = {
    reactStrictMode: true,
    swcMinify: true,
    webpack(config, options) {
        config.plugins.push(
            new NextFederationPlugin({
                name: 'HostApp',
                filename: 'static/chunks/remoteEntry.js',
                remotes: {
                    RemoteApp: `RemoteApp@http://localhost:8080/remoteEntry.js`,
                },
                exposes: {},
                shared: {},
            })
        );
        return config;
    },
}

module.exports = nextConfig;