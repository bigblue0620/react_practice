import React from 'react';

import dynamic from "next/dynamic";

const SimpleButton = dynamic(() => import('RemoteApp/Button').then((comp) => comp.SimpleButton), { ssr: false });
const RemoteSimpleButton = (props) => <SimpleButton {...props} />;

const ComplexButton = dynamic(() => import('RemoteApp/Button').then((comp) => comp.ComplexButton), { ssr: false });
const RemoteComplexButton = () => <ComplexButton />;

const Home = () => {
    return (
        <div>
            <h3>Host App</h3>
            <RemoteSimpleButton buttonText="Host call Remote Btn"></RemoteSimpleButton>
            <RemoteComplexButton />
        </div>
    );
};

export default Home;