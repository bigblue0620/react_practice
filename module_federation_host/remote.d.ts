declare module 'RemoteApp/Button' {
    import { LoaderComponent } from "next/dynamic";

    interface SimpleButtonProps {
        buttonText?: string;
    }
    const SimpleButton: LoaderComponent<SimpleButtonProps>;

    interface ComplexButtonProps { }
    const ComplexButton: LoaderComponent;

    export { SimpleButton, ComplexButton };
}