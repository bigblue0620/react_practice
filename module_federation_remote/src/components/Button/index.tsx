import React, { useState } from 'react';

export const SimpleButton = ({ buttonText = 'SimpleButton' }) => {
    const printHello = () => console.log('Hello from Remote App');
    return <button onClick={printHello}>{buttonText}</button>
};

export const ComplexButton = () => {
    const [count, setCount] = useState(0);

    return (
        <div>
            <span>{count}</span>
            <button onClick={() => setCount(prev => prev + 1)}>Increate Count</button>
        </div>
    );
}