import React from 'react';
import { createRoot } from 'react-dom/client';
import { SimpleButton, ComplexButton } from './components/Button';

class App extends React.Component {
    render() {
        return (
            <div>
                <SimpleButton />
                <ComplexButton />
            </div>
        )
    }
}

const root = createRoot(document.getElementById('root'));
root.render(<App />);