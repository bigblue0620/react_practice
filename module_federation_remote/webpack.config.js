const path = require('path');
const { ModuleFederationPlugin } = require('webpack').container;
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: path.join(__dirname, 'src', 'index.tsx'),
    output: {
        publicPath: 'http://localhost:8080/',
        filename: "bundle.js",
        path: path.resolve(__dirname, 'dist'),
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx|ts|tsx)$/,
                exclude: /node_modules/,
                use: ['babel-loader'] }
        ],
    },
    resolve: {
        extensions: ['.js', '.jsx', '.ts', '.tsx']
    },
    plugins: [
        new ModuleFederationPlugin({
            name: 'RemoteApp',
            library: { type: 'var', name: 'RemoteApp' },
            filename: 'remoteEntry.js',
            exposes: {
                './Button': './src/components/Button/index.tsx'
            },
            shared: {
                react: { singleton: true, eager: true },
                'react-dom': { singleton: true, eager: true }
            },
            remotes: {},
        }),
        new HtmlWebpackPlugin({
            template: 'public/index.html',
        }),
    ],
    performance: {
        hints: false,
        maxEntrypointSize: 512000,
        maxAssetSize: 512000
    },
};
