import React, { useCallback, useEffect } from 'react';
import Chart from "react-apexcharts";
import { makeStyles } from '@material-ui/styles';
import { Box, Link, Tooltip } from '@mui/material';

import { ReactComponent as Disk } from './assets/disk.svg';
import { ReactComponent as SSD } from './assets/ssd.svg';

const useStyles = makeStyles(() => ({
    root: {
        width: '300px',
        height: '130px',
        border: '1px solid gray',
        display:'flex', alignItems:'center', justifyContent: 'flex-start',
        flex: '0 0 auto',
        flexWrap: 'wrap'
    },
    box: {
        display:'flex', alignItems:'center', justifyContent: 'center', width: '40px', height: '40px', borderRadius: '12px', background: '#eaeaea', cursor: 'pointer',
        margin: '4px'
    }
}));

function DiskStatus() {
    const classes = useStyles();   
    
    const tmp: any = [
        {host: 'host-001', type:'disk', status: 'ok'},
        {host: 'host-002', type:'ssd', status: 'failed'},
        {host: 'host-003', type:'disk', status: 'warning'},
        {host: 'host-004', type:'ssd', status: 'ok'},
        {host: 'host-005', type:'disk', status: 'ok'},
        {host: 'host-006', type:'ssd', status: 'warning'},
        {host: 'host-007', type:'disk', status: 'ok'},
        {host: 'host-008', type:'ssd', status: 'ok'},
        {host: 'host-009', type:'disk', status: 'ok'},
        {host: 'host-010', type:'ssd', status: 'ok'},
    ];

    const [data, setData] = React.useState<never[]>(tmp);

    const updateStatus = useCallback(() => {
        const tmp = [].concat(data);
        data.forEach((d: any, i: number) => {
            switch(d.status){
                case 'ok':
                    d.status = 'failed';
                    break;
                case 'failed':
                    d.status = 'ok';
                    break;
                case 'warning':
                    if(i%2 === 0){
                        d.status = 'ok';
                    }
                    break;
            }
        });
        setData([].concat(tmp));
    }, [data]);

    //const data2 = useMemo(() => updateStatus(data), [data]);  

    useEffect(() => {
        const intl = setInterval(() => {
            updateStatus()
        }, 3000);

        return () => { //cleanup
            clearInterval(intl);
        }
    }, [updateStatus]);

    const getStatusElm = (type: string, status: string) => {
        const fillColor = (status === 'ok') ? '#00C853' : (status === 'failed') ? '#D84315' : '#FFC107';
        const el = (type === 'disk') ? <Disk fill={fillColor} /> : <SSD fill={fillColor} />;
        return el;
    }

    return (
        <div className={classes.root}>
            {data.map((d: any, i: number) => (
                <Box className={classes.box} key={`box${i}`}>
                    <Tooltip title={d.host} placement="top">
                        <Link href="#">
                            {getStatusElm(d.type, d.status)}
                        </Link>
                    </Tooltip>
                </Box>
                
            ))}
        </div>
    )
};

function ApexChart() {
    const lineRef = React.createRef();
    const pieRef = React.createRef();

    const time = new Date();
    time.setMilliseconds(0);
    const ts = time.getTime();

    const initData = (ts: number) => {
        const data = [];
        
        // const s = time.getSeconds();
        // if(s !== 0 && s % 2 !== 0){
        //     time.setSeconds(s + 1);
        // }
        
        for (let i=-30; i<0; i++) {
            data.push({
                x: ts + (i * 1000),
                y: 0
            });
        }
        return data;
    }

    // initData 대신 실행 결과 return시 object ref가 일어나는 것 같음
    // const time = new Date();
    // time.setMilliseconds(0);
    // const ts = time.getTime();

    // const tmp = ((ts: number, param?: number) => {
    //     const data = [];
    //     for (let i=-30; i<0; i++) {
    //         data.push({
    //             x: ts + (i * 2000),
    //             y: param || 0
    //         });
    //     }
    //     return data;
    // })(ts);

    const lineOptions: any = {
        options: {
            title: {
                text: "CPU Usage (ApexChart)",
                align: "center",
                style: {
                    fontSize:  '18px',
                    fontWeight:  'normal'
                }
            },
            chart: {
                type: 'line',
                id: "lineChart", //실제 id = apexchartslineChart
                toolbar: {
                    show: false
                },
                animations: {
                    enabled: true,
                    easing: 'linear',
                    dynamicAnimation: {
                        speed: 2000 // data get 인터벌과 동일한 값 설정 필요
                    }
                }
            },
            tooltip: {
                x: {
                    show: true
                },
            },
            xaxis: {
                type: "datetime",
                // type: 'numeric',
                tickAmount: 5,
                // // tickPlacement: 'on', //only works for xaxis.type: category
                labels: {
                    // format: 'HH:mm:ss',
                    //@ts-ignore
                    formatter: function(value, timestamp) {
                        //return new Date(value).toLocaleTimeString();
                        if(value) {
                            const d = new Date(value);
                            return `${('' + d.getHours()).padStart(2, '0')}:${('' + d.getMinutes()).padStart(2, '0')}:${('' + d.getSeconds()).padStart(2, '0')}`;
                        }else {
                            return '';
                        }
                    }
                },
                range: 20000,
                // range: 777600000
            },
            yaxis: {
                min: 0,
                max: 50,
                tickAmount: 5,
                decimalsInFloat: true,
                show: false
            },
            stroke: {
                width: 1.5,
                curve: 'smooth',
            },
            markers: {
                size: 0,
                // colors: '#00f', //마우스 위치했을 때 표시되는 컬러
                strokeColors: '#7CB5EC',
            }
        },
        series: [
            {
                name: "cpu-1",                
                // data: [{x: new Date().valueOf(), y: 0}]
                data: initData(ts)
            },
            {
                name: "cpu-2",
                // data: []
                // data: [{x: new Date(.valueOf(), y: 0}]
                data: initData(ts)
            },
            {
                name: "cpu-3",
                // data: []
                // data: [{x: new Date(.valueOf(), y: 0}]
                data: initData(ts)
            }
        ]
    };

    const hostNum = 5;

    const pieOption: any = {
        series: [1, 4],
        options: {
            chart: {
                width: 380,
                type: 'donut',
            },
            colors: ['#C4C4C4', '#00C853'],
            dataLabels: {
                enabled: true,
                formatter: function (val: number) {
                    return val/(100/hostNum);
                },
            },
            labels: ['Power off', 'Power on'],
            plotOptions: {
                pie: {
                    donut: {
                        labels: {
                            show: true,
                            total: {
                                show: true,
                                // label: '',
                                formatter: function(){
                                    return hostNum;
                                }
                            }
                        }
                    }
                }
            },
            legend: {
                position: 'bottom',
                offsetY: 0,
                height: 30,
            }
        }
    }

    useEffect(() => {
        const asyncRequest = async ()=>{
            //@ts-ignore
            try {
                // const result = await fetch('http://localhost:7779/cpu').then(res => res.json()).then(data => data);
                // // console.log(result);
        
                // //@ts-ignore
                // const chart = lineRef.current.chart;
                // const d = new Date().getTime();
                // chart.appendData([{
                //     data: [{x: d, y: result}]
                // }, {
                //     data: [{x: d, y: result * 0.7}]
                // }, {
                //     data: [{x: d, y: result * 1.7}]
                // }])
            }catch(e){
                console.log("get data fetch error ----------------------");
                clearInterval(intl);
            }
        }

        const asyncRequest2 = async ()=> {
            //@ts-ignore
            const chart = pieRef.current.chart;
            const v = Math.round(Math.random() * 5);            
            chart.updateSeries([v, hostNum - v]);
        }

        const intl = setInterval(() => asyncRequest(), 2000);

        const intlPie = setInterval(() => asyncRequest2(), 3000);

        return () => { //cleanup
            clearInterval(intl);
            clearInterval(intlPie);
        }
    }, [lineRef, pieRef]);
    
    return (
        <div className="App" style={{width: '1000px',height: '100%', display: 'flex'}}>
            <div style={{ paddingLeft: '10px' }}>
                {/* @ts-ignore */}
                <Chart options={lineOptions.options} series={lineOptions.series} type="line" width="770" height="400" ref={lineRef} />
            </div>
            <div>
                {/* @ts-ignore */}
                <Chart options={pieOption.options} series={pieOption.series} type="donut" width="240" height="240" ref={pieRef} />
            </div>
            <DiskStatus />
        </div>
    );
}

export default ApexChart;
