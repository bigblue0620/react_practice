import ApexChart from "./ApexChart";
import HighChart from "./HighChart";

function App() {
    const tmpStyle = {
        width: '1000px',
        height: '100%'
    };
    
    return (
        <div className="App" style={tmpStyle}>
            <br/>
            <ApexChart />
            <br/>
            <br/>
            <HighChart />
        </div>
    );
}

export default App;
