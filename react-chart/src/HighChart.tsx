import React, { Fragment, useEffect } from 'react';
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";

function HighChart() {

    const time = new Date();
    time.setMilliseconds(0);

    const initData = ((time: Date, param?: number) => {
        const data = [];
        for (let i = -4; i <= 0; i++) {
            data.push({
                x: time.getTime() + (i * 2000),
                y: param || 0
            });
        }
        return data;
    })(time);

    const initialOptions: any = {
        title : { text : "CPU Usage (HighChart)" },
        // @ts-ignore
        chart : { type : "spline", animation: Highcharts.svg },
        // chart : { type : "line" },
        yAxis : {
            min: 0,
            max: 50,
            tickAmount: 6,
            title: '',
            visible: false,
        },
        xAxis: {
            type: 'datetime',
            tickAmount: 5,
            tickPixelInterval: 200
        },
        time: {
            timezoneOffset: new Date().getTimezoneOffset()
        },
        plotOptions: {
            series: {
                marker: {
                    enabled: false
                }
            }
        },
        // series : [{
        //     data: [0, 0, 0, 0, 0]
        // }]
        series: [
            {
                name: 'cpu-1',
                data: initData
                // data: [{x: new Date().getTime(), y: 0}]
            },
            {
                name: 'cpu-2',
                data: initData
                // data: [{x: new Date().getTime(), y: 0}]
            },
            {
                name: 'cpu-3',
                data: initData
            },
            {
                name: 'cpu-4',
                data: initData
            }
        ]
    };

    const ch = React.createRef();

    useEffect(() => {
        const asyncRequest = async ()=>{
            try {
                //@ts-ignore
                // const result = await fetch('http://localhost:7779/cpu').then(res => res.json()).then(data => data);
        
                // //@ts-ignore
                // let chart = ch.current.chart;
                // const time = new Date();
                // time.setMilliseconds(0);




                
                // const ts = time.getTime();
                // chart.series[0].addPoint([ts, result], true, true); //The point options, redraw, shift
                // chart.series[1].addPoint([ts, result*0.7], true, true);
                // chart.series[2].addPoint([ts, result*1.2], true, true);
                // chart.series[3].addPoint([ts, result*1.5], true, true);
            }catch(e){
                console.log("get data fetch error ----------------------");
                clearInterval(intl);
            }
        }

        const intl = setInterval(() => asyncRequest(), 2000);

        return () => { //cleanup
            clearInterval(intl);
        }
    }, [ch]);
    
    return (
        <div className="App" style={{width: '770px',height: '400px'}}>
            <Fragment>
                {/* @ts-ignore */}
                <HighchartsReact highcharts={Highcharts} options={initialOptions} allowChartUpdate={true} ref={ch} />
            </Fragment>
        </div>
    );
}

export default HighChart;
