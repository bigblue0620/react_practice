import React from 'react';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';

import Header from './components/Header';
import './components/main.css';

const Home = () => {
	return (
		<div className='contents'>
			Home
		</div>
	);
}

const About = () => {
	return (
		<div className='contents'>
			About
		</div>
	);
}

const Post = ({location}) => {
	return (
		<div className='contents'>
			Post {new URLSearchParams(location.search).get('keyword')}
		</div>
	);
}

const NotFound = () => {
	return (
		<div className='contents'>
			Not Found
		</div>	
	);
}

const App = () => {
	return (
		<Router>
			<div>
				<Header />
				<div>
				    <Switch>
						<Route exact path="/" component={Home}/>
						<Route path="/about" component={About}/>
						<Route path="/post" component={Post}/>
						<Route component={NotFound}/>
					</Switch>
				</div>
			</div>
		</Router>
	);
}

export default App;

/*
	Switch를 쓰면 Route 에서 가장 처음 매칭되는 것만 보여줌
*/