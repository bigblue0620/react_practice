import React from 'react';
import { NavLink } from 'react-router-dom';
import './Header.css';

/*const MenuItem = ({to}) => (
	//<Link to={to} className={`menu-item ${active ? 'active' : ''}`}>{children}</Link>
	<Router>
		<div>
			<Route path={to}></Route>
		</div>
	</Router>
);*/

//<Link => a 태그로 변환됨

const Header = () => {
	return (
		<div>
			<div className="logo">TEST</div>
			<ul className="menu">
		    	<li className='menu-item'><NavLink to="/" activeClassName="active" exact={true}>Home</NavLink></li>
		        <li className='menu-item'><NavLink to="/about" activeClassName="active">About</NavLink></li>
		        <li className='menu-item'><NavLink to="/post" activeClassName="active">Post</NavLink></li>
	        </ul>
	    </div>
	);
}

export default Header;