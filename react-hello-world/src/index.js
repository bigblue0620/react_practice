import React from 'react';
import ReactDOM from 'react-dom';
//import App from './App';
import registerServiceWorker from './registerServiceWorker';
//import './index.css';
import {createStore} from 'redux';

//ReactDOM.render(<App />, document.getElementById('root'));
//registerServiceWorker();

//https://velopert.com/1266
/*
 * store: React.js 프로젝트에서 사용하는 모든 동적 데이터들을 담아두는 곳
	action: 어떤 변화가 일어나야 할 지 나타내는 객체
	reducer: action 객체를 받았을 때, 데이터를 어떻게 바꿀지 처리할지 정의하는 객체
 */
//# react-redux 를 사용하지 않고 만들어보기
const INCREMENT = "INCREMENT";

//action
function increase(diff)
{
	return {type:INCREMENT, addBy:diff}; //type은 필수, action의 형태를 정의, 그 다음부터 자유
}

//Reducer
const initialState = {value : 0};

const counterReducer = (state=initialState, action) => {
	switch(action.type){
		case INCREMENT:
			return Object.assign({}, state, {value : state.value + action.addBy});
		default:
			return state;
	}
}

//Store
const store = createStore(counterReducer);

class App extends React.Component
{
	constructor(props){
		super(props);
		this.onClick = this.onClick.bind(this);
	}
	
	onClick(){
		this.props.store.dispatch(increase(1));
	}

	render(){
		let centerStyle = {
			position: 'fixed',
	        top: '50%',
	        left: '50%',
	        transform: 'translate(-50%, -50%)',
	        WebkitUserSelect: 'none',
	        MozUserSelect: 'none',
	        MsUserSelect:'none',
	        userSelect: 'none',
	        cursor: 'pointer'
		};
		
		return (
			<div onClick={this.onClick} style={centerStyle}>
				<h1>{this.props.store.getState().value}</h1>
			</div>
		);
	}
}

const render = () => {
	ReactDOM.render(<App store={store} />, document.getElementById('root'));
};

//store.subscribe(LISTENER): dispatch 메소드가 실행되면 리스너 함수가 실행. 
//즉, 데이터에 변동이 있을때마다 리렌더링하도록 설정
store.subscribe(render);

render();