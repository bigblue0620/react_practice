import React from 'react';
import PropTypes from 'prop-types';
import './Buttons.css';

const Buttons = ({onCreate, onRemove}) => {
	return (
		<div className="Buttons">
			<div className="btn add" onClick={function(){console.log("Create btn clicked");onCreate();}}>Create</div>
			<div className="btn remove" onClick={onRemove}>Remove</div>
		</div>
	);
};

Buttons.propTypes = {
	onCreate : PropTypes.func,
	onRemove : PropTypes.func
};

Buttons.defaultProps = {
	onCreate : () => console.log("onCreate not defined"),
	onRemove : () => console.log("onRemove not defined"),
};

export default Buttons;