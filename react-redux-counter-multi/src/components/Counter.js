import React from 'react';
import PropTypes from 'prop-types';
import './Counter.css';

const Counter = ({number, color, index, onIncrement, onDecrement, onSetColor}) => {
	return (
		<div
			className="Counter"
			onClick={()=>onIncrement(index)}
			onContextMenu={(e)=>{e.preventDefault();onDecrement(index);}}
			onDoubleClick={()=>onSetColor(index)}
			style={{backgroundColor:color}}>{number}
		</div>	
	); 
};

//Counter를 class 형으로 만들면.
/*class Counter extends React.Component
{
	constructor(props){
		super(props);
		this.onClickProc = this.onClickProc.bind(this);
	}
	
	render(){
		return (
			<div
				className="Counter"
				onClick={this.onClickProc}
				onContextMenu={(e)=>{e.preventDefault();this.props.onDecrement();}}
				onDoubleClick={this.props.onSetColor}
				style={{backgroundColor:this.props.color}}>{this.props.number}
			</div>
		);
	}
	
	onClickProc(e){
		console.log("CLICK !!!!!!!!!!!!");
		this.props.onIncrement();
	}
}*/

Counter.propTypes = {
	number : PropTypes.number,
	color : PropTypes.string,
	onIncrement : PropTypes.func,
	onDecrement : PropTypes.func,
	onSetColor : PropTypes.func
};

Counter.defaultProps = {
	number : 0,
	color: 'black',
    onIncrement: () => console.warn('onIncrement not defined'),
    onDecrement: () => console.warn('onDecrement not defined'),
    onSetColor: () => console.warn('onSetColor not defined')
};

export default Counter;

/*
onContextMenu 마우스 우클릭

*/
