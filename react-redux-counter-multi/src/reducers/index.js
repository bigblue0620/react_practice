//import {combineReducers} from 'redux';
import * as types from '../actions/ActionTypes';
import {Map, List} from 'immutable';

const initialState = Map({
	//counters : [{color:'black', number:0}]//초기치가 있으므로 화면을 열면 타이머가 1개 보임
	//counters : []
	counters : List([Map({color:'black', number:0})])//immutable
});

function counter(state = initialState, action)
{
	const counters = state.get('counters');

	switch(action.type){
		case types.CREATE:
			console.log("onCreate action.color : " + action.color);
			return state.set('counters', counters.push(Map({color:action.color, number:0})));
		case types.REMOVE:
			return state.set('counters', counters.pop());
		case types.INCREMENT:
			return state.set('counters', counters.update(action.index, (counter)=>counter.set('number', counter.get('number')+1)));
		case types.DECREMENT:
			return state.set('counters', counters.update(action.index, (counter)=>counter.set('number', counter.get('number')-1)));
		case types.SET_COLOR:
			return state.set('counters', counters.update(action.index, (counter)=>counter.set('color', action.color)));
		default:
			console.log("reducer init");
			return state;
	}
};

export default counter;
