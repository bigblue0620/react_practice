//import {combineReducers} from 'redux';
import * as types from '../actions/ActionTypes';
import {Map, List} from 'immutable';

const initialState = {
	//counters : [{color:'black', number:0}]//초기치가 있으므로 화면을 열면 타이머가 1개 보임
	//counters : []
	counters : List([Map({color:'black', number:0})])//immutable
};

function counter(state = initialState, action)
{
	const {counters} = state;
	
	switch(action.type){
		case types.CREATE:
			return {
				counters : [
					...counters,
					{color : action.color, number : 0}
				]
			};
		case types.REMOVE:
			return {
				counters : counters.slice(0, counters.length -1)
			};
		case types.INCREMENT:
			return {
				counters : [
					...counters.slice(0, action.index),
					{
						...counters[action.index],
						number : counters[action.index].number + 1
					},
					...counters.slice(action.index+1, counters.length)
				]
			};
		case types.DECREMENT:
			return {
				counters : [
					...counters.slice(0, action.index),
					{
						...counters[action.index],
						number : counters[action.index].number - 1
					},
					...counters.slice(action.index+1, counters.length)
				]
			};
		case types.SET_COLOR:
			return {
				counters : [
					...counters.slice(0, action.index),
					{
						...counters[action.index],
						color : action.color
					},
					...counters.slice(action.index+1, counters.length)
				]
			};
		default:
			console.log("reducer init");
			return state;
	}
};

export default counter;