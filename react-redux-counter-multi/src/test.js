var Map = Immutable.Map;

/*var data = Map({
	a: 1,
	b: 2,
	c: Map({
	    d: 3,
	    e: 4,
	    f: 5
    })
});

console.log(data.toJS());
console.log(JSON.stringify(data.toJS()));
console.log(data.get('a'));
console.log(data.getIn(['c', 'd']));

var newData = data.set('a', 4);

console.log(JSON.stringify(data.toJS()));
console.log(JSON.stringify(newData.toJS()));

var newData = data.setIn(['c', 'd'], 10);
console.log(JSON.stringify(newData.toJS()));

//var newData = data.mergeIn(['c'], { d: 10, e: 10 });
var newData = data.setIn(['c', 'd'], 10).setIn(['c', 'e'], 10);
console.log(JSON.stringify(newData.toJS()));

var newData = data.merge({ a: 10, b: 10 });
console.log(JSON.stringify(newData.toJS()));*/


var List = Immutable.List;
var list = List([0,1,2,3,4]); //단순 값일 때
var list2 = List([Map({age:11, name:'jane'}), Map({age:33, name:'tom'})]);//객체일 때

console.log(list.get(0));
console.log(list2.getIn([0, 'name']));
var list3 = list2.setIn([0, 'name'], 'chone');
console.log(list2.getIn([0, 'name']));
console.log(list3.getIn([0, 'name']));

//아이템 수정하기 update or setIn
var list4 = list3.update(0, item=>item.set('name', item.get('name') + 'aaaa'));
console.log(list4.getIn([0, 'name']));
var list5 = list3.setIn([0, 'name'], list3.getIn([0, 'name']) + 'aaaa');
console.log(list5.getIn([0, 'name']));
var list6 = list3.set(0, Map({value:10}));//아이템 자체를 수정해버림 {age:11, name:'chone'} -> {value:10}
console.log(JSON.stringify(list6.toJS()));

var list7 = list.push(Map({value:3}));
console.log(JSON.stringify(list7.toJS()));
var list8 = list.unshift(Map({value:3}));
console.log(JSON.stringify(list8.toJS()));
var list9 = list8.delete(0);
console.log(JSON.stringify(list9.toJS()));
var list10 = list9.pop();
console.log(JSON.stringify(list10.toJS()));
console.log(list10.size);
console.log(list10.isEmpty());