/*
 컨테이너 컴포넌트를 store 에 연결을 시켜주려면 react-redux의 connect 함수를 사용해야하는데요, 
 이 함수의 파라미터로 컴포넌트에 연결시킬 상태와, 액션함수들을 전달해주면, 컴포넌트를 
 리덕스 스토어에 연결시키는 또 다른 함수를 반환합니다. 
 이 과정에서 리턴된 함수 안에, 프리젠테이셔널 컴포넌트를 파라미터로 전달해주면 
 리덕스 스토어에 연결된 컴포넌트가 새로 만들어집니다.

 컴포넌트에 연결시킬 상태와 액션함수를 정의할땐 각각 함수를 만들어줘야하는데요. 
 상태를 연결시킬땐 state, 액션함수를 연결시킬땐 dispatch 를 파라미터로 전달받는 함수를 만들어서 
 객체를 반환하면 이를 props 로 사용 할 수 있게 됩니다.
*/

import Counter from '../components/Counter';
import * as actions from '../actions';
import {connect} from 'react-redux';
import {getRandomColor} from '../utils';

//store 안의 state 값을 props 로 연결해줍니다.
const mapStateToProps = (state) => ({
	color : state.colorReducer.color,
	number : state.numberReducer.number
});

const mapDispatchToProps = (dispatch) => ({
	onIncrement : () => {console.log("action dispatch 111"); dispatch(actions.increment())},
	onDecrement : () => dispatch(actions.decrement()),
	onSetColor : () => {dispatch(actions.setColor(getRandomColor()));}
});

const CounterContainer = connect(mapStateToProps, mapDispatchToProps)(Counter);

export default CounterContainer;