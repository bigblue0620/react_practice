//import * as types from '../actions/ActionTypes';
import numberReducer from './numberReducer';
import colorReducer from './colorReducer';
import {combineReducers} from 'redux';

/*const initialState = {
	color : 'black',
	number : 0
};

function counter(state = initialState, action)
{
	switch(action.type){
		case types.INCREMENT:
			console.log("22222");
			return {...state, number : state.number+1};
		case types.DECREMENT:
			return {color : state.color, number : state.number-1};
		case types.SET_COLOR:
			return {...state, color : action.color};
		default:
			return state;
	}
};*/

const counter = combineReducers({numberReducer, colorReducer});

export default counter;