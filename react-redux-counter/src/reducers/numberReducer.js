import * as types from '../actions/ActionTypes.js';

const initialState = {number:0};

const numberReducer = (state=initialState, action) => {
	switch(action.type){
		case types.INCREMENT:
			console.log("numberReducer 22222");
			return {number : state.number + 1};
		case types.DECREMENT:
			return {number : state.number - 1};	
		default:
			return state;
	}
}

export default numberReducer;