import React, { Component } from 'react';
import CounterDisplay from './components/CounterDisplay';
import DiffInput from './components/DiffInput';
import Spinner from './components/Spinner';

class App extends Component {
  render() {
    return (
      <div style={{textAlign:'center'}}>
          <CounterDisplay />
          <DiffInput />
          <Spinner />
      </div>
    );
  }
}

export default App;
