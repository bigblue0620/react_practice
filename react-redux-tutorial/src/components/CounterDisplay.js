import React from 'react';
import {connect} from 'react-redux';

class CounterDisplay extends React.Component
{
	render(){
		return (
			<h1>VALUE : {this.props.value}</h1>
		);
	}
}

let mapStateToProps = (state) => {
	return {value : state.counter.value};
}

CounterDisplay = connect(mapStateToProps)(CounterDisplay);

export default CounterDisplay;
/*
connect는 react-redux 의 내장 API. 
이 함수는 React Component 를 Redux Store에 ‘연결’.

이 함수의 리턴값은 특정 컴포넌트 클래스의 props 를 store의 데이터에 연결시켜주는 
또 다른 함수를 리턴. 리턴된 함수에 컴포넌트를 인수로 넣어 실행하면, 
기존 컴포넌트를 수정하는게 아니라 새로운 컴포넌트를 return

mapStateToProps(state, [ownProps]): (Function) store 의 state 를 컴포넌트의 props 에 매핑
ownProps 인수가 명시될 경우, 이를 통해 함수 내부에서 컴포넌트의 props 값에 접근.
*/