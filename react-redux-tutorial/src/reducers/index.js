import {INCREMENT, DECREMENT, SET_DIFF} from '../actions';
import {combineReducers} from 'redux';

const counterInitialState = {value:0, diff:1};

const counter = (state=counterInitialState, action) => {
	switch(action.type){
		case INCREMENT:
			return Object.assign({}, state, {value:(state.value + state.diff)});
		case DECREMENT:
			return Object.assign({}, state, {value:(state.value - state.diff)});
		case SET_DIFF:
			return Object.assign({}, state, {diff:action.diff});
		default:
			return state;
	}
};

const extra = (state={value:'this is extra reducer'}, action) =>{
	switch(action.type){
		default:
			return state;
	}
};

const counterApp = combineReducers({counter, extra});

/* 이 코드와 동일함.
const counterApp = ( state = { }, action ) => {
    return {
        counter: counter(state.counter, action),
        extra: extra(state.extra, action)
    }
}
 */

export default counterApp;

//combineReducers 는 여러개의 reducer를 한개로 합칠 때 사용 되는 redux 내장 메소드
//이 예제에서는 딱히 여러개의 reducer 를 사용 할 필요가 없었으므로, 예제로만 작성
//위의 reducer 를 사용하여 store를 만들게 되면, store 의 state 구조는 다음과 같이 생성
/*
{
    counter: { value: 0, diff: 1 }
    extra: { value: 'this_is_extra_reducer' }
}
reducer를 여러개로 분리하여 작성 할 땐, 서로 직접적인 관계가 없어야 함

combineReducers 를 사용 할 때, 각 reducer에 다른 key를 주고싶다면 다음과 같이 작성
const counterApp = combineReducers({
    a: counter,
    b: extra
});
*/