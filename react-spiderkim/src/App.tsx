import React, { Fragment } from 'react';

import { Route } from 'react-router-dom';
import { Home, Signin } from './home';
import Dashboard from './dashboard/dashboard';
import { CircularProgress, makeStyles, Modal } from '@material-ui/core';
import { RootStateOrAny, useSelector } from 'react-redux';

//TODO withStyles
//https://velog.io/@youthfulhps/%EB%A6%AC%EB%8D%95%EC%8A%A4-%EB%AF%B8%EB%93%A4%EC%9B%A8%EC%96%B4%EB%8A%94-%EB%AC%B4%EC%97%87%EC%9D%B8%EA%B0%80-2

const useStyles = makeStyles({
    modal: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",

        '& .MuiCircularProgress-root': {
            outline: 'none'
        },

        '& .MuiCircularProgress-colorPrimary': {
            color: '#333'
        }
    }
});

function App() {
    const classes = useStyles();
    const loading = useSelector((state: RootStateOrAny) => (state.common.loading));

    return (
        <Fragment>
            <Route path="/" component={Home} exact />
            <Route path="/signin" component={Signin} />
            <Route path="/dashboard" component={Dashboard} />
            <Modal open={loading} className={classes.modal}>
                <CircularProgress />
            </Modal>
        </Fragment>
    );
}

export default App;


//https://react.vlpt.us/react-router/01-concepts.html