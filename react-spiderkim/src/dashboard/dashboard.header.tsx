import React from 'react';
import { AppBar, Toolbar, IconButton, makeStyles, Button, Box } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { Typography } from '@material-ui/core';
import { RootStateOrAny, useSelector } from 'react-redux';
import { Link } from 'react-router-dom'

const DashboardHeader = () => 
{
    const classes = makeStyles((theme) =>
    ({
        menuButton: {
          marginRight: theme.spacing(2),
        },        
        title: {
            flexGrow: 1,
            textAlign: 'right',
            fontSize: '16px'
        },
        verticalLine: {
            marginLeft: theme.spacing(1)
        },
        button: {
            fontSize: '16px',
            textTransform: 'none'
        },
        userIcn: {
            marginRight: theme.spacing(1)
        },
        box: {
            marginLeft: 'auto'
        }
    }))();

    const email = (useSelector((state: RootStateOrAny) => (state.auth.user.email))) || '';

    return (
        <AppBar position="static">
            <Toolbar>
                <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                    <MenuIcon />
                </IconButton>
                <Box display="flex" alignItems="center" className={classes.box}>
                    <AccountCircleIcon className={classes.userIcn} />
                    <Typography className={classes.title}>{email}</Typography>
                    <Typography className={classes.verticalLine}>|</Typography>
                    <Button color="inherit" className={classes.button} component={Link} to='/signin'>Logout</Button>
                </Box>
            </Toolbar>
        </AppBar>
    );
};

export default DashboardHeader;