import React from 'react';
import { Route } from 'react-router-dom';
import DashboardHeader from './dashboard.header';
import Project from './project';

const Dashboard = () =>
{
    const tmpStyle = {
        width: '100%',
        height: '100%'
    };

    return (
        <div style={tmpStyle}>
            <DashboardHeader></DashboardHeader>
            <Route path="/dashboard" component={Project} exact/>
            <Route path="/dashboard/project" component={Project} exact />
        </div>
    );
};

export default Dashboard;