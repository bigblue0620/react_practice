import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { errorAlert } from '../helper/util';
// import { setMembershipInfo } from '../redux/auth';
import { projectList } from '../service/project';
import { DataGrid } from '@material-ui/data-grid';
import { Box, Button, makeStyles } from '@material-ui/core';


const columns: any[] = [
    { field: 'id', headerName: 'ID', hide: true },
    { field: 'title',headerName: '프로젝트명', width: 100},
    { field: 'url', headerName: 'URL',width: 214},
    { field: 'finished_at', headerName: '최종 실행', width: 153 },
    { field: 'status', headerName: '상태', width: 84 },
    { field: 'next_date', headerName: '다음 실행', width: 153 },
    { field: 'schedule', headerName: '예약 등록', width: 100},
    { field: 'result', headerName: '최근 결과물', width: 200},
    { field: 'all_result', headerName: '모든 결과', width: 100,
        renderCell: () => (
            <Button >보기</Button>
        )
    }
];

for(const el of columns){
    el['editable'] = false;
    el['sortable'] = false;
}

const paging = {
    itemPerPage: 5,
    itemPerPhase: 25,
    phase: 5,
    currentPage: 1,
    currentPhase: 1,
    total: 0,
    showPageArrowBtn: false,     //first, previous, next, last 버튼 표시 유무
    pageNumArr: []
};

const useStyles = makeStyles({
    root: {
        marginTop: '100px',
        padding: '0 50px',
        height: '396px',
        maxWidth: '1250px'
    },
    title: {
        fontSize: '16px',
        marginBottom: '10px',
        lineHeight: 1
    },
    dataGrid: {
        '& .MuiDataGrid-columnHeaderTitleContainer': {
            justifyContent: 'center'
        },

        '& .MuiDataGrid-columnSeparator': {
            display: 'none'
        },

        '& .MuiDataGrid-cell': {
            textAlign: 'center'
        }
    }    
});

const Project = () =>
{
    let pList: React.IProject[]= [];
    const [len, setLen] = useState(0);
    const [rows, setRows] = useState([{id: 0}]);
    const classes = useStyles();

    const getProjectList = async () => {
        projectList(new URLSearchParams({page: `${paging.currentPage}`, size: `${paging.itemPerPage}`}).toString())
        .then(res => {
            pList = res.value.content;
            setLen(res.value.total);
            makeRows();
            //dispatch(setMembershipInfo(res.value));
        }).catch(err => {
            errorAlert(err);
        });
    }

    useEffect(() => {
        getProjectList();
    }, []);

    useEffect(() => {
        //getProjectList();

        const intl = setInterval(() => getProjectList(), 7000);

        return () => { //cleanup
            clearInterval(intl);
        }
    }, []);

    const makeRows = () => {
        const tmp:any[] = [];
        for(const el of pList){
            const obj:any = {};
            
            /*const keys: string[] = Object.keys(el);
            for(const prop of keys){
                obj[prop] = el[prop] || '-';
            }*/

            obj.id = el.project_id;
            obj.title = el.title;
            obj.finished_at = (el.task?.finished_at || '').replace('T', ' ');
            obj.status = el.task?.status;
            obj.next_date = el.schedule?.next_date || '-';
            obj.schedule = 'schedule';
            obj.result = '';
            obj.all_result = '';
            tmp.push(obj);
        }
        setRows([...tmp]);
    }

    return (
        <Box display="flex" flexDirection="column" className={classes.root}>
            <div className={classes.title}>Total: {len}</div>
            <DataGrid rows={rows} columns={columns} pageSize={5} rowsPerPageOptions={[5, 10, 20]}
                    checkboxSelection={true} disableColumnMenu={true} disableSelectionOnClick={true} className={classes.dataGrid} showColumnRightBorder={false} />
        </Box>
    );
};

export default Project;