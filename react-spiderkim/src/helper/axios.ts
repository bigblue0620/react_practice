// import React from 'react';
import axios from 'axios';
import { getAccessToken } from './util';
// import { ErrorCode } from './enum';

axios.defaults.baseURL = process.env.REACT_APP_SERVER_DOMAIN

axios.interceptors.request.use(
    (config) => {
        const token = getAccessToken();
        if(token){
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    }
);

axios.interceptors.response.use(
    (response) => {
        return response.data;
    },
    (error) => {
        // console.log(error);
        // console.log(error.response);
        // if(error.response.data.result === ErrorCode.EXPIRED_ACCESS_TOKEN){
        //     const refToken = getRefreshToken();
        //     console.log(refToken);
        //     return;
        // }
        return Promise.reject(error.response.data);
    } 
);

export default axios;