export enum ErrorCode
{
    DUPLICATED_EMAIL = '602',       //이미 정상 가입 완료된 Email
    BAD_CREDENTIALS = '611',        //password 불일치
    NOT_FOUND_USER = '400',         //가입 정보(Not Found User)를 찾을 수 없는 경우(로그인시 400 응답)
    NOT_FOUND_EMAIL = '614',        //가입 정보(Not Found Email)를 찾을 수 없는 경우
    NOT_VERIFY_EMAIL = '612',       //이메일 미인증
    BLOCKED_EMAIL = '604',          //관리자가 차단한 회원
    DELETED_EMAIL = '605',          //(이미)탈퇴한 회원
    RESTRAINED_EMAIL = '606',       //탈퇴 후 재가입 기준일(30일)이 지나지 않은 Email
    INVALID_PW = '616',             //비밀번호 형식이 잘못된 경우
    NOT_MATCHED_PW = '617',         //비밀번호, 비밀번호 확인값이 일치하지 않는 경우
    INCORRECT_PW = '615',           //등록된 비밀번호와 일치하지 않는 경우

    INVALID_TOKEN = '701',
    EXPIRED_TOKEN = '702',

    INVALID_ACCESS_TOKEN = '711',
    EXPIRED_ACCESS_TOKEN = '712',
    
    INVALID_REFRESH_TOKEN = '721',
    EXPIRED_REFRESH_TOKEN = '722'
};