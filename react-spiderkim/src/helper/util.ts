import i18n from '../i18n/i18n';


export function errorAlert(err: React.IResponse): void
{
    if(err && err.result){
        const msgKey = err.result;
        const msg = (msgKey && i18n.t(`ERROR.CODE_${msgKey}`)) || i18n.t('ERROR.COMMON');
        alert(msg);
    }
}

const ACCESS_TOKEN = 'ACCESS_TOKEN';
const REFRESH_TOKEN = 'REFRESH_TOKEN';

export function saveToken (value: React.ITokens): void
{
    localStorage.setItem(ACCESS_TOKEN, value.access_token);    
    localStorage.setItem(REFRESH_TOKEN, value.refresh_token);
}

export function getAccessToken(): string | null {
    return localStorage.getItem(ACCESS_TOKEN);
}

export function getRefreshToken(): string | null {
    return localStorage.getItem(REFRESH_TOKEN);
}

export function logout(): void
{
    localStorage.removeItem(ACCESS_TOKEN);
    localStorage.removeItem(REFRESH_TOKEN);
}