import React, { FormEvent, useState } from 'react';
import { makeStyles, Box, Button, TextField, Typography } from '@material-ui/core';
// import styled from 'styled-components';
// import { Link } from 'react-router-dom';
import { useTranslation  } from 'react-i18next';
// import imgLogo from '../assets/logo.png';
import { errorAlert, logout, saveToken } from '../helper/util';
import { useDispatch } from 'react-redux';
import { setUser, /*setMembershipInfo,  me, membership,*/ signin } from '../redux/auth';
import { useEffect } from 'react';
import { endLoading, startLoading } from '../redux/common';
// import { ThunkAppDispatch } from '../redux';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles({
    root: {
        height: '100%'
    },
    logo: {
        marginTop: '200px',
        marginBottom: '40px'
    },
    textfield: {
        width: '500px',
        height: '50px',
        marginBottom: '10px',

        '& .MuiInputBase-root': {
            borderRadius: 0,
            lineHeight: 1
        },

        '& .MuiInputBase-input': {
            padding: '0 0 0 20px',
            height: '50px',
            fontSize: '14px'
        },

        '& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline': {
            borderColor: 'rgba(0, 0, 0, 0.23)',
            borderWidth: '1px'
        }
    },
    button: {
        height: '50px',
        borderRadius: 0,
        fontFamily: 'IBMPlexSansKR-Medium',
        fontSize: '16px',
        //backgroundColor: '#f41631 !important',
        color: '#fff',
        // borderWidth: '0 !important',

        // '&:hover': {
            
        // }
    },
    modal: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",

        '& .MuiCircularProgress-root': {
            outline: 'none'
        },

        '& .MuiCircularProgress-colorPrimary': {
            color: '#333'
        }
    }
});

const Signin = () =>
{
    useEffect(() => {
        logout(); //localStorage clear
        dispatch(endLoading());
        dispatch(setUser(null));
        // dispatch(setMembershipInfo(null));
    }, []);

    const { t } = useTranslation();
    //const dispatch: ThunkAppDispatch = useDispatch();
    const dispatch = useDispatch();

    const classes = useStyles();


    const [email, setEmailId] = useState('');
    const [password, setPw] = useState('');

    const changeEmailId = (e: React.ChangeEvent<HTMLInputElement>) => setEmailId(e.target.value);
    const changePw = (e: React.ChangeEvent<HTMLInputElement>) => setPw(e.target.value);

    
    // const hasError = () => {
    //     return (email.length < 5) ? true : false;
    // }

    // const login = (e: React.MouseEvent) => {
    // TODO hasError가 발생한 상태에서도 submit은 동작함, required 에러일 경우에는 막힘(=submit 동작이 일어나지 않음)
    //
    const doSingin = (e: FormEvent) => 
    {
        e.preventDefault();

        const formData = new FormData();
        formData.append("email", email);
        formData.append("password", password);

        dispatch(signin(formData));

        // dispatch(singin(formData))
        // .then((res: React.IResponse) => {
        //     saveToken(res.value);
        //     //getProfile();
        // }).catch((err: React.IResponse) => {
        //     errorAlert(err);
        // });
    }

    /*const getProfile = () => {
        dispatch(me())
        .then(()=> {
            getMembership();
        })
        .catch((err: React.IResponse) => {
            errorAlert(err);
        });
    }

    const history = useHistory();

    const getMembership = () => {
        dispatch(membership())
        .then(() => {
            history.push('/dashboard');
        })
        .catch((err: React.IResponse) => {
            errorAlert(err);
        });
    }*/

    return (
        <Box display="flex" flexDirection="column" justifyContent="flexStart" alignItems="center" className={classes.root}>
            {/*<Link to="/">*/}
                {/*<img src={imgLogo} width="200px" height="40px" alt="logo" className={classes.imgLogo}/>*/}
                <Typography variant="h2" className={classes.logo}>Login</Typography>
            {/*</Link>*/}
            <form onSubmit={doSingin}>
                <Box display="flex" flexDirection="column">
                    <TextField onChange={changeEmailId} placeholder={t('ID')} autoFocus={true} required={true} variant="outlined" className={classes.textfield} 
                        // error={hasError()}
                        // helperText={
                        //     hasError() ? "입력한 비밀번호와 일치하지 않습니다." : null
                        // }
                        />
                    <TextField type="password" onChange={changePw} placeholder={t('PW')} autoFocus={false} required={true} variant="outlined" className={classes.textfield} />
                    <Button color="primary" variant="contained" type="submit" className={classes.button}>{t('SIGNIN')}</Button>
                </Box>
            </form>            
        </Box>
    );
}

export default Signin;

//리액트 progress
//https://basketdeveloper.tistory.com/65