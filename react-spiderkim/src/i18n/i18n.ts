import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import ko from './ko';
import en from './en';

const resources = {ko, en};

i18n.use(initReactI18next).init({resources, lng: "ko"});

export default i18n;