const ko = {
    translation: {
        ID: '아이디',
        PW: '비밀번호',
        SIGNIN: '로그인',
        ERROR: {
            CODE_602 : '이미 가입된 이메일입니다. 본인 아이디라면 비밀번호 찾기를 해주세요.', //Duplication Email Error(이미 정상 가입 완료된 Email)
            CODE_604 : '약관 위반으로 인해 이용이 제한된 회원입니다', //Blocked Email Error            
            CODE_605 : '탈퇴한지 한 달 이내의 아이디는 로그인이 제한됩니다.\n다른 아이디를 입력해주세요.', //Deleted(Withdrawn) Email Error
            CODE_606 : '탈퇴한 이메일은 한 달간 재가입이 불가능합니다.', //Restrained Email Error(탈퇴 후 재가입 기준일(30일)이 지나지 않은 Email)
            CODE_611 : '비밀번호가 일치하지 않습니다.', //Bad credentials
            CODE_612 : '인증 대기 중인 이메일 입니다.', //Not Verified Email Error
            CODE_614 : '미등록된 아이디(이메일)입니다.',//Not Found Email
            CODE_400 : '미등록된 아이디(이메일)입니다.',//Not Found User
            CODE_615 : '기존 비밀번호가 일치하지 않습니다.',//Incorrect Password(등록된 비밀번호와 일치하지 않을 경우)
            CODE_701 : '이메일 인증 정보가 유효하지 않습니다. 로그인 후 다시 이메일 인증을 신청해 주세요.', //Invalid Verification Token
            CODE_702 : '이메일 인증 유효 시간이 초과되었습니다. 로그인 후 다시 이메일 인증을 신청해 주세요.', //Expired Verification Token
            COMMON: "에러가 발생했습니다."
        }
    }
}

export default ko;