import React from 'react';
// import { ThunkAppDispatch } from './';
// import { signin as doSignin, me as getProfile, membership as getMembership } from '../service/auth';
// import { endLoading } from './common';

// const SIGNUP_START = 'auth/SIGNUP_START';
// const SIGNUP_SUCCESS = 'auth/SIGNUP_SUCCESS';
// const SIGNUP_ERROR = 'auth/SIGNUP_ERROR';

// const START_USER = 'auth/START_USER';
// const SET_USER = 'auth/SET_USER';
// const ERROR_USER = 'auth/ERROR_USER';
// const GET_USER = 'auth/GET_USER';

// const START_MEMBERSHIP_INFO = 'auth/START_MEMBERSHIP_INFO';
// const SET_MEMBERSHIP_INFO = 'auth/SET_MEMBERSHIP_INFO';
// const GET_MEMBERSHIP_INFO = 'auth/GET_MEMBERSHIP_INFO';
// const ERROR_MEMBERSHIP_INFO = 'auth/ERROR_MEMBERSHIP_INFO';

// export const setUser = (info: React.IUser | null) => ({type: SET_USER, info});
// export const getUser = () => ({type: GET_USER});
// export const setMembershipInfo  = (info: React.IMemberShip | null) => ({type: SET_MEMBERSHIP_INFO, info});
// export const getMembershipInfo = () => ({type: GET_MEMBERSHIP_INFO});

// const initialState = {
//     user: null,
//     membership: null
// };

// export default function auth(state = initialState, action: React.IAction)
// {
//     switch(action.type){
//         case SET_USER:
//             return {
//                 ...state,
//                 user: {...action.info}
//             };
//         case GET_USER:
//             return {...state}['user'];
//         case SET_MEMBERSHIP_INFO:
//             return {
//                 ...state,
//                 membership: {...action.info}
//             };
//         case GET_MEMBERSHIP_INFO:
//             return {...state}['membership'];
//         default:
//             return state;
//     }
// }



// export const singin = (formData: FormData) => async (dispatch: ThunkAppDispatch): Promise<React.IResponse> => 
// {
//     dispatch({type: SIGNUP_START});

//     return new Promise((resolve, reject) => {
//         doSignin(formData)
//         .then((res: React.IResponse) => {
//             resolve(res);            
//             dispatch({type: SIGNUP_SUCCESS});
//         }).catch((err: React.IResponse) => {
//             reject(err);
//             dispatch({type: SIGNUP_ERROR});
//             dispatch(endLoading());
//         });
//     });
// }

// export const me = () => async (dispatch: ThunkAppDispatch) => 
// {
//     dispatch({type: START_USER});

//     return new Promise((resolve, reject) => {
//         getProfile()
//         .then((res: React.IResponse) => {
//             resolve(null);
//             console.log(res.value);
//             dispatch(setUser(res.value));
//             dispatch(membership());
//         }).catch((err: React.IResponse) => {
//             reject(err);
//             dispatch({type: ERROR_USER});
//             dispatch(endLoading());
//         });
//     });
// }

// export const membership = () => async (dispatch: ThunkAppDispatch) => 
// {
//     dispatch({type: START_MEMBERSHIP_INFO});

//     return new Promise((resolve, reject) => {
//         getMembership()
//         .then((res: React.IResponse) => {
//             resolve(null);
//             dispatch(setMembershipInfo(res.value));
//             dispatch(endLoading());
//         }).catch((err: React.IResponse) => {
//             reject(err);
//             dispatch({type: ERROR_MEMBERSHIP_INFO});
//             dispatch(endLoading());
//         });
//     });
// }

import * as authAPI from '../service/auth';
import { call, put, takeEvery } from 'redux-saga/effects';
import { errorAlert, saveToken } from '../helper/util';
import { LOADING_END, CHANGE_ROUTE, LOADING_START } from './common';

const SIGNUP_START = 'auth/SIGNUP_START';
const SET_USER = 'auth/SET_USER';
const GET_USER = 'auth/GET_USER';
const SET_MEMBERSHIP_INFO = 'auth/SET_MEMBERSHIP_INFO';
const GET_MEMBERSHIP_INFO = 'auth/GET_MEMBERSHIP_INFO';

export const signin = (formData: FormData) => ({type: SIGNUP_START, payload: formData});
export const setUser = (payload: React.IUser | null) => ({type: SET_USER, payload});
export const getUser = () => ({type: GET_USER});
export const getMembershipInfo = () => ({type: SET_MEMBERSHIP_INFO});
export const setMembershipInfo = (payLoad: React.IMemberShip) => ({type: SET_MEMBERSHIP_INFO, payLoad});

function* singinAndGetUserInfo(action: {type: string, payload: FormData | null})
{
    try{
        yield put({type: LOADING_START});
        let res: React.IResponse = yield call(<any>authAPI.signin, action.payload);
        yield call(saveToken, res.value);
        res = yield call(authAPI.me);
        yield put({type: SET_USER, payload: res.value});
        res = yield call(authAPI.membership);
        yield put({type: SET_MEMBERSHIP_INFO, payload: res.value});
        yield put({type: CHANGE_ROUTE, payload: '/dashboard/project'});
    }catch(e){
        console.log(e);
        yield call(errorAlert, e);
    }finally{
        yield put({type: LOADING_END});
    }
}

export function* authSaga() {
    yield takeEvery(SIGNUP_START, singinAndGetUserInfo);
}

const initialState = {
    user: null,
    membership: null
};

export default function auth(state = initialState, action: React.IAction)
{
    console.log(JSON.stringify(action));
    switch(action.type){
        case SET_USER:
            return {
                ...state,
                user: {...action.payload}
            };
        case GET_USER:
            return {...state}['user'];
        case SET_MEMBERSHIP_INFO:
            return {
                ...state,
                membership: {...action.payload}
            };
        case GET_MEMBERSHIP_INFO:
            return {...state}['membership'];
        default:
            return state;
    }
}