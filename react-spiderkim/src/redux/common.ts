import React from 'react';
import { getContext, takeEvery } from 'redux-saga/effects';
import { History } from 'history';

export const LOADING_START = 'common/LOADING_START';
export const LOADING_END = 'common/LOADING_END';
export const CHANGE_ROUTE = 'common/CHANGE_ROUTE';

export const startLoading = () => ({type: LOADING_START});
export const endLoading = () => ({type: LOADING_END});
export const changeRoute = (param: string) => ({type: CHANGE_ROUTE, param});

const initialState = {
    loading: false
};

export default function common(state = initialState, action: React.IAction)
{
    switch(action.type){
        case LOADING_START:
            return {
                ...state,
                loading: true
            };
        case LOADING_END:
            return {
                ...state,
                loading: false
            };
        default:
            return state;
    }
}

export function* changeRouteHistory(action: {type: string, payload: string})
{
    const history: History = yield getContext('history');
    history.push(action.payload);
}

export function* commonSaga() {
    yield takeEvery(CHANGE_ROUTE, changeRouteHistory);
}