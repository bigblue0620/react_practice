import { createStore, applyMiddleware } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { combineReducers } from 'redux';
import auth, { authSaga } from './auth';
import project, { projectSaga } from './project';
import common, { commonSaga } from './common';
import { composeWithDevTools } from 'redux-devtools-extension';
import logger from 'redux-logger';
// import ReduxThunk from 'redux-thunk';
// import { ThunkDispatch } from "redux-thunk";
// import { AnyAction } from "redux";
import { all } from 'redux-saga/effects';
import createSagaMiddleware from 'redux-saga'
import { createBrowserHistory, History } from 'history';

export const customHistory: History = createBrowserHistory();

const persistConfig = {
    key: 'root',
    storage
};

const rootReducer = combineReducers({
    auth,
    project,
    common
});

export function* rootSaga() {
    yield all([authSaga(), projectSaga(), commonSaga()]);
}

const persistedReducer  = persistReducer(persistConfig, rootReducer);
const sagaMiddleware = createSagaMiddleware({
    context: {
        history: customHistory
    }
});
const store = createStore(persistedReducer, composeWithDevTools(applyMiddleware(sagaMiddleware, logger)));
sagaMiddleware.run(rootSaga);

export default store;

// export type RootState = ReturnType<typeof store.getState>
// export type ThunkAppDispatch = ThunkDispatch<RootState, void, AnyAction>;