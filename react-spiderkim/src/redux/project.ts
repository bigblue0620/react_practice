import * as projectAPI from '../service/project';
import { call, put, takeEvery } from 'redux-saga/effects';
import { errorAlert, saveToken } from '../helper/util';
import React from 'react';

const GET_PROJECT = 'auth/GET_PROJECT';

export const getProject = (payload: string) => ({type: GET_PROJECT, payload});

function* getProjectList(action: {type: string, payload: string | null})
{
    try{
        
    }catch(e){
        console.log(e);
        yield call(errorAlert, e);
    }/*finally{
        yield put({type: LOADING_END});
    }*/
}

export function* projectSaga() {
    yield takeEvery(GET_PROJECT, getProjectList);
}

const initialState = {
    project: null
};

export default function project(state = initialState, action: React.IAction)
{
    switch(action.type){
        default:
            return state;
    }
}