import axios from '../helper/axios';

// interface response {
//     data?: {
//         result: string;
//         message?: string;
//         value: any;
//     },
//     config?: any;
//     headers?: any;
//     request?: XMLHttpRequest;
// }

export async function signin(formData: FormData): Promise<React.IResponse> {
    return await axios.post('/api/v1.0/auth/signin', formData);
}

export async function me(): Promise<React.IResponse> {
    return await axios.get('/api/v1.0/me');
}

export async function membership(): Promise<React.IResponse> {
    return await axios.get('/api/v1.0/me/membership');
}