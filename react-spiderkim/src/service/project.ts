import axios from '../helper/axios';

export async function projectList(param: string): Promise<React.IResponse> {
    return await axios.get(`/api/v1.0/projects?${param}`);
}