import 'react';

declare module "react"
{
    export interface IAction {
        type: string;
        payload?: {
            [key: string]: any;
        }
    }

    export interface ITokens {
        access_token: string;
        access_token_expires_at?: string;
        refresh_token: string;
    }

    export interface IResponse {
        result: string;
        message?: string;
        value: any; // succeed
        error: any; // failed
    }

    export interface IUser {
        member_no: number;
        email: string;
        nickname: string;
        status: string;
        is_validated: boolean;
        plan_type: string;
    }

    interface IPlanCommon {
        id: number;
        type: string;
        max_project: number;
        max_page?: number;
        retention_period?: number;
        auto_proxy?: boolean;
        scheduling?: boolean;
        support_faq?: boolean;
        support_email?: boolean;
        support_realtime?: boolean;
        support_onsite?: boolean;
        period?: number;                // 이용 기간 [ 0(무제한) | 1 | 30 | 90 ]
        start_date?: null | string;
        end_date?: null | string;
        status?: string;
        title?: string;
        expires_in?: number;
    }
    
    interface IMemberShip {
        plan: IPlanCommon,
        project?: {
            total?: number;
            registered?: number;
            processing?: number;
            failed?: number;
            completed?: number;
            scheduled?: number;
        }
    }

    interface IProjectCommon {
        statusLbl?: string;         //상태에 따른 문자열
        enableFileDown?: boolean;   //파일 다운로드 가능 상태 체크값
        selectBoxOpen?: boolean;    //모바일 - 화면에서 파일 다운로드 셀렉트 박스 open 상태
        fileDownType?: string;      //모바일 - 다운로드 선택한 파일 타입
    }
    
    interface IProject extends IProjectCommon {
        project_id: string;
        member_id?: number;
        type?: string;      //REGULAR | TEMPLATE | CUSTOM 
        title?: string;
        host?: string;
        url: string;
        rule?: string;
        task?: ITask;
        schedule?: ISchedule;
        created_at?: string;    //최초 등록 시간
        updated_at?: string;    //마지막 변경 시간. 참조용
        expired?: boolean;
        //------- frontend 측에서 추가한 속성 ---------- 
        titleLbl?: string;    
        isChecked?: boolean;
        enableSchedule?: boolean;   //예약(스케줄링) 가능 여부
    }

    interface ITask extends IProjectCommon {
        project_id?: string;
        task_id?: string;
        type?: string;
        status?: string;
        url?: string;
        max_page?: number;
        max_row?: number;
        created_at?: string;    //최초 등록 시간
        updated_at?: string;    //마지막 변경 시간. 참조용
        started_at?: string;    //마지막 실행 시작 시간
        finished_at?: string;   //마지막 실행 종료 시간
        retains_at?: string;    //결과 데이터 보관 만료 일시
        is_expired?: boolean;
        results? : {
            EXCEL?: boolean;
            CSV?: boolean;
            JSON?: boolean;
        }
    }
    
    interface ISchedule {
        project_id?: string;
        type?: string;
        days?: string[];
        hour?: string;
        minute?: string;
        end_date: string;
        next_date?: string;
    }
}