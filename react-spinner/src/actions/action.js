import * as types from './actionType';

export const incrementAct = () => ({
    type : types.INCREMENT
});

export const decrementAct = () => ({
    type : types.DECREMENT
});
