import React from 'react';
import {connect, complete} from 'react-redux';

let InputBox = ({number}) => {
    return (
        <div>
            <input type="text" value={number} />
        </div>
    );
};

let mapStateToProps = (state) => {
    return {number : state.number};
};

InputBox = connect(mapStateToProps)(InputBox);
export default InputBox;
