import React from 'react';
import {connect} from 'react-redux';
import {incrementAct, decrementAct} from '../actions/action';

/*let SpinnerBtn = ({onIncrement, onDecrement, number, min, max}) => {
    return (
        <div>
            <button type="button" onClick={onDecrement, onMouseUp} onMouseDown={onMouseDown} onMouseUp={onMouseUp} disabled={number == min}>-</button>
            <button type="button" onClick={onIncrement, onMouseUp} onMouseDown={onMouseDown} onMouseUp={onMouseUp} disabled={number == max}>+</button>
        </div>
    );
};*/

let spinnerInterval = null;
class SpinnerBtn extends React.Component
{
	constructor(props){
		super(props);
		this.onClickProc = this.onClickProc.bind(this);
        this.onMouseUpProc = this.onMouseUpProc.bind(this);
        this.onMouseDownProc = this.onMouseDownProc.bind(this);
        //this.state = { mouseDowned : false };
	}

	render(){
		return (
            <div>
                <button type="button" onClick={this.onClickProc} onMouseDown={()=>{this.onMouseDownProc(false)}} onMouseUp={this.onMouseUpProc} disabled={this.props.number == this.props.min}>-</button>
                <button type="button" onClick={this.onClickProc} onMouseDown={()=>{this.onMouseDownProc(true)}} onMouseUp={this.onMouseUpProc} disabled={this.props.number == this.props.max}>+</button>
            </div>
		);
	}

	onClickProc(e){
        //console.log("click");
		this.props.onIncrement();
        this.onMouseUpProc();
	}

    onMouseDownProc(isInc){
        //console.log("mouse down : " + isInc);
        if(!event){
            return;
        }

        if(!spinnerInterval){
            var that = this;
            var thatIsInc = isInc;
            spinnerInterval = setInterval((() => {
                that.props[(thatIsInc)? "onIncrement" : "onDecrement"]();
            }), 300);
        }
    }

    onMouseUpProc(e){
        //console.log("mouse up");
        if(!event){
            return;
        }
        //this.state = { mouseDowned : false };
        clearInterval(spinnerInterval);
        spinnerInterval = null;
    }
}

let mapStateToProps = (state) => {
    return {number:state.number, min:state.min, max:state.max};
};

let mapDispatchToProps = (dispatch) => {
    return {
        onIncrement : () => dispatch(incrementAct()),
        onDecrement : () => dispatch(decrementAct())
    };
};

SpinnerBtn = connect(mapStateToProps, mapDispatchToProps)(SpinnerBtn);
export default SpinnerBtn;
