import React from 'react';
import InputBox from '../components/InputBox';
import SpinnerBtn from '../components/SpinnerBtn';

class App extends React.Component
{
    render(){
        return (
            <div>
                <InputBox />
                <SpinnerBtn />
            </div>
        )
    }
};

export default App;
