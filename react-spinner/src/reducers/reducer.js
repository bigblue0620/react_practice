import * as types from '../actions/actionType';

const initialState = {number:0, min:0, max:100};

function spinner(state = initialState, action)
{
    switch(action.type){
        case types.INCREMENT:
            return { number : ((state.number >= state.max) ? state.number : state.number + 1), min:state.min, max:state.max};
        case types.DECREMENT:
            return { number : ((state.number <= state.min) ? state.number : state.number - 1), min:state.min, max:state.max};
        default:
            return state;
    }
}

export default spinner;
