import * as types from './actionTypes';

export const create = (name) => ({type : types.CREATE, name : name});
export const remove = (index) => ({type : types.REMOVE, index : index});
export const complete = (index) => ({type : types.COMPLETE, index : index});//개별 항목 completed 처리
export const allChange = () => ({type : types.ALL_CHANGE});
export const viewAll = () => ({type : types.VIEW_ALL});
export const viewActive = () => ({type : types.VIEW_ACTIVE});
export const viewComplete = () => ({type : types.VIEW_COMPLETE});
export const removeComplete = () => ({type : types.REMOVE_COMPLETE});
