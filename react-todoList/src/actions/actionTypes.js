export const CREATE = 'CREATE';
export const REMOVE = 'REMOVE';
export const COMPLETE = 'COMPLETE';
export const ALL_CHANGE = 'ALL_CHANGE';
export const VIEW_ALL = "VIEW_ALL";
export const VIEW_ACTIVE = "VIEW_ACTIVE";
export const VIEW_COMPLETE = 'VIEW_COMPLETE';
export const REMOVE_COMPLETE = 'REMOVE_COMPLETE';
