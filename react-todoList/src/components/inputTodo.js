import React from 'react';
import {connect} from 'react-redux';
import {create, allChange} from '../actions/action';
import ReactDOM from 'react-dom';

class InputTodo extends React.Component
{
	constructor(props){
		super(props);
		this.onKeyPress = this.onKeyPress.bind(this);
	}

	render(){
		return (
            <div id="inputBoxDiv">
				{this.props.len > 0 ? <input type="checkbox" className="toggle-all" checked={this.props.allComplete ? true : false} onClick={this.props.onAllChange}/> : null}
                <input type="text" autoFocus ref={ref => this.todoInput = ref} className="new-todo" placeholder="What needs to be done?" onKeyPress={this.onKeyPress}/>
            </div>
		);
	}

	onKeyPress(e){
        if(e.charCode == 13){
            this.props.onCreate(e.target.value);
            this.todoInput.value = "";
        }
	}
}

let mapStateToProps = (state) => {
	return {len : state.get('todos').size, allComplete : state.get('allComplete')};
};

let mapDispatchToProps = (dispatch) => {
    return {
        onCreate : (val) => dispatch(create(val)),
		onAllChange : () => dispatch(allChange())
    };
};

InputTodo = connect(mapStateToProps, mapDispatchToProps)(InputTodo);
export default InputTodo;
