import React from 'react';
import {connect} from 'react-redux';
import {remove, complete, viewAll, viewActive, viewComplete, removeComplete} from '../actions/action';
import {Map, List} from 'immutable';

let TodoList = ({todos, view, onRemove, onComplete, onViewAll, onViewActive, onViewComplete, onRemoveComplete}) => {
    const todoItemList = todos.map((todo, i) => (
        <TodoItem key={i} index={i} {...todo.toJS()} view={view} onRemove={onRemove} onComplete={onComplete} />));

	return (
        <div id="listDiv">
            <ul className="todo-list">
                {todoItemList}
            </ul>
            {todos.size > 0 ? <TodoListFooter todos={todos} onViewAll={onViewAll} onViewActive={onViewActive} onViewComplete={onViewComplete} onRemoveComplete={onRemoveComplete} /> : null}
        </div>
	);
};

const TodoItem = ({index, name, complete, view, onRemove, onComplete}) => {
    var viewCondition = (view == VIEW.ALL) ? true : (view == VIEW.ACTIVE) ? !complete : complete;
    return (
        <li className={complete ? "completed" : ""} style={{display: viewCondition ? 'block' : 'none'}}>
            <div className="view">
                <input type="checkbox" className="toggle" checked={complete ? true : false} onClick={()=>onComplete(index)}/>
                <label>{name}</label>
                <button className="destroy" onClick={()=>onRemove(index)}></button>
            </div>
        </li>
	);
};

let TodoListFooter = ({todos, onViewAll, onViewActive, onViewComplete, onRemoveComplete}) => {
    var activeCount = 0;
    todos.map((todo) => {if(todo.get('complete') == false){activeCount++;}});

    const clickLi = function(idx){
        const nodeList = document.querySelectorAll('ul.filters li > a');
        for (var i = 0; i < nodeList.length; ++i) {
            nodeList[i].className = (i == idx) ? "selected" : ""
        }
    }

    return (
        <div>
            <footer className="footer">
                <span className="todo-count">{activeCount} items left</span>
                <ul className="filters">
                    <li><a href="#/" className="selected" onClick={()=>{clickLi(0);onViewAll();}}>All</a></li>
                    <li><a href="#/Active" onClick={()=>{clickLi(1);onViewActive();}}>Active</a></li>
                    <li><a href="#/Complete" onClick={()=>{clickLi(2);onViewComplete();}}>Complete</a></li>
                </ul>
                {todos.size != activeCount ? <button className="clear-completed" onClick={onRemoveComplete}>Clear completed</button> : null}
            </footer>
        </div>
    );
};

let mapStateToProps = (state) => {
    console.log("state.get('todos') size : " + state.get('todos').size);
    return {todos : state.get('todos'), view : state.get('view')};
};

let mapDispatchToProps = (dispatch) => {
    return {
        onRemove : (index) => dispatch(remove(index)),
        onComplete : (index) => dispatch(complete(index)),
        onViewAll : () => dispatch(viewAll()),
        onViewActive : () => dispatch(viewActive()),
        onViewComplete : () => dispatch(viewComplete()),
        onRemoveComplete : () => dispatch(removeComplete())
    };
};

TodoList = connect(mapStateToProps, mapDispatchToProps)(TodoList);
export default TodoList;
