import React from 'react';
import InputTodo from '../components/InputTodo';
import TodoList from '../components/TodoList';

window.VIEW = {
	ALL : "all",
	ACTIVE : "active",
	COMPLETE : "complete"
}

class App extends React.Component
{
    render(){
        return (
            <div>
                <InputTodo />
                <TodoList />
            </div>
        )
    }
};

export default App;
