import * as types from '../actions/actionTypes';
import {Map, List} from 'immutable';

const initialState = Map({
	todos : List([]), //Map({index:0, complete:false, name : ""})
	allComplete : false,
	view : VIEW.ALL
});

function todoList(state = initialState, action)
{
	const todos = state.get('todos');
	const len = todos.size;

	switch(action.type){
		case types.CREATE:
            return state.set('todos', todos.push(Map({index:new Date().getTime(), name:action.name, complete:false})));
		case types.REMOVE:
			var index = todos.findIndex(todo => todo.get('index') == action.index);
			return state.set('todos', todos.delete(index));
		case types.COMPLETE:
			var index = todos.findIndex(todo => todo.get('index') == action.index);
			var newTodos = todos.update(index, todo => todo.set('complete', !todo.get('complete')));
			var completeCount = 0;
			newTodos.map((todo) => {if(todo.get('complete') == true){completeCount++;}});
			return Map({todos : newTodos, allComplete : (completeCount == len), view : state.get('view')});
		case types.ALL_CHANGE:
			var allComplete = !(state.get('allComplete'));
			var newTodos = todos.map((todo) => todo.set('complete', allComplete));
			return Map({todos : newTodos, allComplete : allComplete, view : state.get('view')});
		case types.VIEW_ALL:
			return state.set('view', VIEW.ALL);
		case types.VIEW_ACTIVE:
			return state.set('view', VIEW.ACTIVE);
		case types.VIEW_COMPLETE:
			return state.set('view', VIEW.COMPLETE);
		case types.REMOVE_COMPLETE:
			return state.set('todos', todos.filter((todo) => {
		        var temp = todo.toJS();
		        return !temp.complete;
		    }));
		default:
			return state;
	}
};

export default todoList;
