import * as types from '../actions/actionTypes';
//import {Map, List} from '../../node_modules/immutable/dist/immutable';
import {Map, List} from 'immutable';

const initialState = Map({
	todos : List([]), //Map({index:0, complete:false, name : ""})
	allComplete : false,
	count : 0
});

function todoList(state = initialState, action)
{
	const todos = state.get('todos');
	const len = todos.size;
	if(len != 0){//TODO delete
		console.log(JSON.stringify(todos.toJS()));
	}

	switch(action.type){
		case types.CREATE:
            console.log("CREATE =============  name : " + action.name);
			return state.set('todos', todos.push(Map({index:new Date().getTime(), name:action.name, complete:false})));
		case types.REMOVE:
			console.log("REMOVE ============== index : " + JSON.stringify(action.index));
			//return state.set('todos', todos.delete(action.index)); //배열 항목이 삭제되므로 index 값이 맞지 않게 됨
			/*return state.set('todos', state.get('todos').filter((todo) => {//filter로 모든 항목을 돌아야 하므로 비효율적
		        var temp = todo.toJS();
		        return temp.index != action.index;
		    }));*/
			var index = todos.findIndex(todo => todo.get('index') == action.index);
			/*var index = todos.findIndex(function(todo){
				return todo.get('index') == action.index;
			})*/
			console.log("index : " + index);
			return state.set('todos', todos.delete(index));
		case types.ALL:
			return state.set('allComplete', null);
		case types.ACTIVE:
			return state.set('allComplete', false);
		case types.COMPLETE:
			console.log("COMPLETE ============== index : " + JSON.stringify(action.index));
            var index = todos.findIndex(todo => todo.get('index') == action.index);
			console.log(index);
			return state.set('todos', todos.update(index, todo => todo.set('complete', !todo.get('complete'))));
		default:
			console.log("reducer init");
			//console.log(state);
			return state;
	}
};

export default todoList;
