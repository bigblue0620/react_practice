import React from 'react';//mport 는 ES6 에 도입된 새 문법, var React = require(‘react’)

//class 는 ES6 에 새로 도입된 요소
//모든 Component는 React.Component 를 상속
//ES5 환경에서는 React.createClass() 라는 메소드를 사용
class App extends React.Component
{
	sayHello(){
		alert("Hello");
	};

	render(){
		let tmp = "chone"
		return (
			<div>
				<h1>Hello {tmp} </h1>
				<button onClick={this.sayHello}>click</button>
			</div>	
		);//React JSX 는 XML-like Syntax 를 native Javascript로 변환
		//만약 this.sayHello에 ()이 붙으면 로딩될 때 실행됨
	};//만약 <h1>이 하나 더 있다면 즉 여러 엘리먼트가 있을 경우에는 <div>등으로 감싸야 함.
	//JSX에서 자바스크립트를 사용하려면 {}로 감싸면 됨
};

export default App;

//JSX 안에서 사용되는 JavaScript 표현에는 If-Else 문이 사용 불가
//삼항연산자는 OK
//JSX 안에서 주석을 작성할 때엔, { /* comments */ } 형식으로 작성



//JSX 파일의 확장자의 경우, 이전에는 개발자들이 .jsx 확장자를 사용
//요즘은 .js 를 사용하는 추세로 전환

//JSX 안 쓴 버전
/*
var Hello = React.createClass({
	render: function() {
  		return React.createElement("div", null, "Hello ", this.props.name);
	}
});

ReactDOM.render(React.createElement(Hello, {name: "World"}),document.getElementById('container'));
*/

//JSX 쓴 버전
/*
var Hello = React.createClass({
  render: function() {
    return <div>Hello {this.props.name}</div>;
  }
});

ReactDOM.render(<Hello name="World" />, document.getElementById('container'));
*/
