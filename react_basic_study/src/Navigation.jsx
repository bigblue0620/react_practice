// @file Navigation.jsx
/*(function(React) {

    // 보여줄 링크
    var linkList = [
        {
            title: "쿠팡", href :"http://www.coupang.com/"
        },
        {
            title: "아마존", href :"http://www.amazon.com/"
        },
        {
            title: "11번가", href :"http://www.11st.co.kr/"
        },
        {
            title: "Yes24", href :"http://www.yes24.com/"
        }
    ];

    var Navigation = React.createClass({
        links: function() {
            var self = this;
            return linkList.map(function(link) {
                return (
                    <li><a href={link.href} target={link.target || "_self"}>{link.title}</a></li>
                );
            });
        },
        render: function() {
            return (
                <ul classname="nav">
                    {this.links()}
                </ul>
            );
        }
    });

    React.render( <Navigation/>, document.querySelector(".nav") );

})(React);*/

//위에 처럼해도 되고 이렇게 해도 되고
var linkList = [{title: "쿠팡", href :"http://www.coupang.com/"},
        		{title: "아마존", href :"http://www.amazon.com/"},
        		{title: "11번가", href :"http://www.11st.co.kr/"},
        		{title: "Yes24", href :"http://www.yes24.com/"}];

var Navigation = React.createClass(
{
	links: function() {
		var self = this;
        return linkList.map(function(link) {
        	return (
            	<li><a href={link.href} target={link.target || "_self"}>{link.title}</a></li>
        	);
        });
	},
	render: function() {
    	return (<ul classname="nav">{this.links()}</ul>);
	}
});

React.render( <Navigation/>, document.querySelector(".nav") );

 