import React from 'react';
import Header from './Header';
import Content from './Content';
import RandomNumber from './RandomNumber';

class App extends React.Component
{
	constructor(props){
		super(props);
		this.state = {
			value : Math.round(Math.random()*100)
		};
		this.updateValue = this.updateValue.bind(this);
	}
	
	updateValue(randomValue){
		//console.log("randomValue : " + randomValue);
		this.setState({value : randomValue});
	}
	
	render(){
		return (
			<div>
				<Header title={this.props.headerTitle}/>
				<Content title={this.props.contentTitle} body={this.props.contentBody} />
				<RandomNumber number={this.state.value} onUpdate={this.updateValue} />
			</div>	
		);
	};
};

App.defaultProps = {
    headerTitle: 'Default header',
    contentTitle: 'Default contentTitle',
    //contentTitle: '5',
    contentBody: 'Default contentBody'
    //contentBody: undefined
};


/*
parent 컴포넌트에 의해 값이 변경 될 수 있는가?      Yes         No
컴포넌트 내부에서 변경 될 수 있는가?                 No         Yes                          
 */

export default App;