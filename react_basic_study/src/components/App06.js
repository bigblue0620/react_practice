import React from 'react';
import Header from './Header';
import Content from './Content';
import RandomNumber from './RandomNumber';
import update from 'react-addons-update';

class App extends React.Component
{
	render(){
		return (
			<Contacts />	
		);
	};
};

class Contacts extends React.Component
{
	constructor(props){
		super(props);
		this.state = {
			contactData : [
				{name: "Abet", phone: "010-0000-0001"},
                {name: "Betty", phone: "010-0000-0002"},
                {name: "Charlie", phone: "010-0000-0003"},
                {name: "David", phone: "010-0000-0004"}
			],
			selectedKey : -1,
			selected : {
				name : "",
				phone : ""
			}
		}
	}
	
	_insertContact(name, phone){
		let newState = update(this.state, {
			contactData : {
				$push : [{"name":name, "phone":phone}]
			}
		});
		this.setState(newState);
	}
	
	_onSelect(key){
		if(key == this.state.selectedKey){
			console.log("key select canceled");
			this.setState({selectedKey : -1,
							selected : {
								name : "",
								phone : ""
							}});
			return;
		}
		
		this.setState({selectedKey : key, selected : this.state.contactData[key]});
		console.log(key + " is selected");
	}
	
	_isSelected(key){
		return (this.state.selectedKey == key);
	}
	
	_removeContact(){
		if(this.state.selectedKey == -1){
			console.log("contact not selected");
			return;
		}
		
		this.setState({
			contactData : update(
				this.state.contactData, {$splice : [[this.state.selectedKey, 1]]}
			),
			selectedKey : -1
		});
	}
	
	_editContact(name, phone)
	{
		this.setState({
			contactData : update(
				this.state.contactData,
				{
					[this.state.selectedKey] : {
						name : {$set : name},
						phone : {$set : phone}
					}
				}
			),
			selected : {name : name, phone : phone}
		});
	}

	render(){
		return (
			<div>
                <h1>Contacts</h1>
                <ul>
                	{this.state.contactData.map((contact, i) => {
                		return (<ContactInfo name={contact.name} 
                							phone={contact.phone} 
                							key={i} 
                							contactKey={i}
                							onSelect={this._onSelect.bind(this)}
                							isSelected={this._isSelected.bind(this)(i)}
                							/>);
                	})}
                </ul>
                <ContactCreator onInsert={this._insertContact.bind(this)} />
                <ContactRemover onRemove={this._removeContact.bind(this)} />
                <ContactEditor onEdit={this._editContact.bind(this)} 
                				isSelected={(this.state.selectedKey != -1)} 
                				contact={this.state.selected} />
            </div>
		);
	}
}

class ContactInfo extends React.Component
{
	handleClick(){
		//contactKey 는 해당 컴포넌트의 고유 번호로 사용함
		//컴포넌트를 매핑할 때 key 를 사용하였으나, key는 prop으로 간주되지 않으며 
		//React 내부에서 사용하는 용도이기에 직접 접근이 불가
		this.props.onSelect(this.props.contactKey);//contactKey - Contacts에서 ContactInfo 만들때 props로 넣어줌
	}
	
	shouldComponentUpdate(nextProps, nextState){
		return (JSON.stringify(nextProps) != JSON.stringify(this.props));
	}
	
    render()
    {
    	console.log("rendered: " + this.props.name);

    	let getStyle = (isSelect) => {
    		if(!isSelect) return;
    		
    		return {fontWeight : 'bold', backgroundColor : '#4efcd8'};
    	}
    	
        return(
            <li onClick={this.handleClick.bind(this)} 
            	style={getStyle(this.props.isSelected)}>{this.props.name} {this.props.phone}</li>
        );
    }
}

class ContactCreator extends React.Component
{
	constructor(props){
		super(props);
		this.state = {name:"", phone:""};
	}

	handleChange(e){
		var nextState = {};
		nextState[e.target.name] = e.target.value;
		this.setState(nextState);
	}
	
	handleClick(){
		this.props.onInsert(this.state.name, this.state.phone);//parent에서 props 받아온 메소드 실행
		this.state = {name:"", phone:""};
	}
	
	render(){
		return (
			<div>
				<p>
					<input type="text" 
							name="name" 
							placeholder="name" 
							value={this.state.name} 
							onChange={this.handleChange.bind(this)} />&nbsp;&nbsp;
                    <input type="text" 
                    		name="phone" 
                    		placeholder="phone" 
                    		value={this.state.phone} 
							onChange={this.handleChange.bind(this)} />&nbsp;&nbsp;
                    <button onClick={this.handleClick.bind(this)}>Insert</button>
                    {/*value={this.state.name} 이렇게 해두면 인풋 박스에 텍스트를 적으려고 해도 안됨.*/}
				</p>
			</div>
		);
	}
}

class ContactRemover extends React.Component
{
	handleClick(){
		this.props.onRemove();
	}

	render(){
		return (
			<button onClick={this.handleClick.bind(this)}>Remove Selected contact</button>
		);
	}
}

class ContactEditor extends React.Component
{
	constructor(props){
		super(props);
		this.state = {name:"", phone:""};
	}
	
	handleClick(){
		if(!this.props.isSelected){
			console.log("Contact not selected");
			return;
		}
		this.props.onEdit(this.state.name, this.state.phone);
	}
	
	handleChange(e){
		var nextState = {};
		nextState[e.target.name] = e.target.value;
		this.setState(nextState);
	}
	
	componentWillReceiveProps(nextProps){
		//console.log("===== componentWillReceiveProps =====");
		//console.log(nextProps);
		//console.log(nextProps.contact.name);
		//console.log(nextProps.contact.phone);

		this.setState({
			name : nextProps.contact.name,
			phone : nextProps.contact.phone
		});
	}
	
	render(){
		return (
			<div>
				<p>
					<input type="text"
							name="name"
							placeholder="name"
							value={this.state.name}
							onChange={this.handleChange.bind(this)} />&nbsp;&nbsp;
					<input type="text"
							name="phone"
							placeholder="phone"
							value={this.state.phone}
							onChange={this.handleChange.bind(this)} />&nbsp;&nbsp;
					<button onClick={this.handleClick.bind(this)}>Edit</button>				
				</p>
					
			</div>
		);
	}
}

export default App;