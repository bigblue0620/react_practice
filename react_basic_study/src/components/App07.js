import React from 'react';

/*class App extends React.Component
{
	render(){
		return(
			<div>
				{//<input ref="myInput" />}
				<input ref={ref=>(this.input = ref)} />
			</div>
		);
	}

	componentDidMount(){
		//this.refs.myInput.value = "Hi, I used ref to do this";
		this.input.value = "Test";
	}
}*/

/*class App extends React.Component
{
	//ref에 DOM 객체를 넘겨줌.
	render(){
		return(
			<div>
				<TextBox ref={(TextBox) => (this.textBoxUnit = TextBox)} />
				<button onClick={this.handleClick.bind(this)}>Click</button>
			</div>
		);
	}

	handleClick(){
		this.textBoxUnit.inputUnit.value = "I used ref";
	}
}

class TextBox extends React.Component
{
	render(){
		return(
			<input ref={(input) => (this.inputUnit = input)} onKeyPress={this.keyInput.bind(this)} />
		);
	}
	
	keyInput(){
		//console.log(this.inputUnit);
	}
}*/

class App extends React.Component
{
	render(){
		return(
			<div>
				<input ref={input => this.input = input} />
				<button onClick={this.handleClick.bind(this)}>Click</button>
			</div>
		);
	}
	
	handleClick(){
		this.input.value = "";
		this.input.focus();
	}
}

export default App;