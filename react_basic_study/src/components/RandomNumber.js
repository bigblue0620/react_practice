import React from 'react';

class RandomNumber extends React.Component
{
	constructor(props){
		super(props);
		this.updateNumber = this.updateNumber.bind(this);
	}
	
	updateNumber(){
		let value = Math.round(Math.random()*100);
		this.props.onUpdate(value);
	}
	
	render(){
		return (
			<div>
				<h1>Random Number : {this.props.number}</h1>
				<button onClick={this.updateNumber}>Randomize</button>
			</div>
		);
	}
};

export default RandomNumber;

/*
	두가지 prop 사용
	number
	onUpdate  <- parent의 함수를 props로 받아서 실행함.
*/