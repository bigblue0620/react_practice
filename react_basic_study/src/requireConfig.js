require.config({
	paths: {
		"react": "../node_modules/react/lib/React",
		"create-react-class" : "../node_modules/create-react-class/create-react-class",
		"react-dom" : "../node_modules/react-dom/lib/ReactDom",
		"prop-types" : "../node_modules/prop-types/prop-types"
		//'object-assign': "../node_modules/object-assign/index",
	},
	shim : {
		'react' : {
			//deps: ['object-assign']
		}
	}
});