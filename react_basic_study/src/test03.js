var React = require('react');
//console.log(React);

window.ReactDOM = require('react-dom');
var createReactClass = require('create-react-class');
//console.log(createReactClass);

/*class Hello extends React.Component {

	  render() {
	    return <div>Hello, {this.props.name}!</div>
	  }
	}*/



var linkList = [{title: "쿠팡", href :"http://www.coupang.com/"},
	{title: "아마존", href :"http://www.amazon.com/"},
	{title: "11번가", href :"http://www.11st.co.kr/"},
	{title: "Yes24", href :"http://www.yes24.com/"}];

var Navigation = createReactClass(
{
	links: function() {
		var self = this;
		return linkList.map(function(link) {
			return (
					<li key={link.href}><a href={link.href} target={link.target || "_self"}>{link.title}</a></li>
			);
		});
	},
	render: function() {
		return (<ul className="nav">{this.links()}</ul>);
		//return (<ul>{this.links()}</ul>);
	}
});

//React.render( <Navigation/>, document.querySelector(".nav") );
ReactDOM.render( <Navigation/>, document.querySelector(".nav") );

//browserify test03.js -o result.js -t babelify --presets [react es2015] //not worked
//browserify -t reactify test03.js > result.js
//browserify -t [ reactify --es6 ] main.js