import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App03';

/*class App extends React.Component {
    render(){

        return (
                <h1>Hello React Skeleton</h1>
        );
    }
}*/

//export default App;//같은 파일 안에 있을 때는 없어도 됨

const rootElement = document.querySelector(".nav")
ReactDOM.render(<App />, rootElement);

//browserify -t [babelify] test04.js > result.js