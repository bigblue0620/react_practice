import WebpackDevServer from 'webpack-dev-server';
import webpack from 'webpack';
import express from 'express';
import path from 'path';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import session from 'express-session';
import api from './routes';

const app = express();
const port = 3000;
const devPort = 4000;

app.use('/', express.static(path.join(__dirname, './../public')));

app.get('/hello', (req, res) => {
    return res.send('Hello CodeLab');
});

app.listen(port, () => {
    console.log('Express is listening on port', port);
})

if(process.env.NODE_ENV == 'development'){
    console.log('Server is running on development mode');
    const config = require('../webpack.dev.config');
    const compiler = webpack(config);
    const devServer = new WebpackDevServer(compiler, config.devServer);
    devServer.listen(devPort, () => {console.log('webpack-dev-server is listening on port', devPort)});
}

app.use(morgan('dev'));
app.use(bodyParser.json());

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost:27017/codelab', {useMongoClient: true});
const db = mongoose.connection;
db.on('error', console.error);
db.once('open', () => { console.log('---------------Connected to mongodb server'); });

app.use(session({ secret:'CodeLab1$1$234', resave:false, saveUninitialized:true }));

//이렇게 하면 http://localhost/api/account/signup 이런식으로 api 사용 가능
//api == ./routes/index.js의 router == ./routes/account.js 의 router
app.use('/api', api);

app.use(function(err, req, res,next){ //라우터에서 throw err가 발생하면 이 코드가 실행됨
    console.error(err.stack);
    res.status(500).send('Something broke!');
})

app.get('*', (req, res) => {res.sendFile(path.resolve(__dirname, './../public/index.html'))});
