import express from 'express';
import account from './account';
import memo from './memo';

const router = express.Router();
router.use('/account', account);
router.use('/memo', memo);// api/memo 에다가 GET / POST / PUT / DELETE 등 메소드로 요청

export default router;
