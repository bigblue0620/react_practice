import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

class Header extends React.Component
{
    render(){
        const loginButton = (
            <li>
                <Link to="/login">
                    <i className="material-icons">vpn_key</i>
                </Link>
            </li>
        );
        /*
        Link 이 컴포넌트는 페이지를 새로 로딩하는것을 막고, 라우트에 보여지는 내용만 변하게
             만약에 a 태그로 이동을하게된다면 페이지를 처음부터 새로 로딩
        */

        const logoutButton = (
            <li>
                <a onClick={this.props.onLogout}><i className="material-icons">lock_open</i></a>
            </li>
        );

        return (
            <nav>
                <div className="nav-wrapper blue darken-1">
                    <Link to="/" className="brand-logo center">MEMOPAD</Link>
                    <ul>
                        <li><a><i className="material-icons">search</i></a></li>
                    </ul>
                    <div className="right">
                        <ul>
                            {this.props.isLoggedIn ? logoutButton : loginButton}
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}

Header.propTypes = {
    isLoggedIn : PropTypes.bool,
    onLogout : PropTypes.func
}

Header.defaultProps = {
    isLoggedIn : false,
    onLogout : () => { console.error("logout function not defined"); }
}

export default Header;
