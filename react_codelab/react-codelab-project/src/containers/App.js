import React from 'react';
import {Header} from '../components/';
import Home from './Home';
import {connect} from 'react-redux';
import {getStatusRequest, logoutRequest} from '../actions/authentication';

/*
컴포넌트를 생성 할 때는 constructor -> componentWillMount -> render -> componentDidMount 순으로 진행

*/
class App extends React.Component
{
    constructor(props){
        super(props);
        this.handleLogout = this.handleLogout.bind(this);
    }

    componentDidMount()//render 후에 불려짐, 즉 새로 고침 후에 로그인 상태인지 아닌지 체크함
    {
        function getCookie(name){
            var value = '; ' + document.cookie;
            var parts = value.split('; ' + name + '=');
            if(parts.length == 2) return parts.pop().split(';').shift();
        }

        let loginData = getCookie('key');

        if(typeof loginData == 'undefined') return;

        loginData = atob(loginData);

        if(loginData == 'undefined') return;//type == 'string'

        loginData = JSON.parse(loginData);

        if(!loginData.isLoggedIn) return;

        this.props.getStatusRequest()
        .then(() => {
            //console.log(this.props.status);
            if(!this.props.status.valid){
                loginData = {
                    isLoggedIn : false,
                    username : ''
                };

                document.cookie = 'key=' + btoa(JSON.stringifyloginData);

                let $toastContent = $('<span style="color: #FFB4BA">Your session is expired, please log in again</span>');
                Materialize.toast($toastContent, 4000);
            }
        });
    }

    handleLogout()
    {
        this.props.logoutRequest()
        .then(() => {
            Materialize.toast('Good Bye!', 2000);

            let loginData = {isLoggedIn:false, username:''};
            document.cookie = 'key=' + btoa(JSON.stringify(loginData));
        })
    }

    render(){
        let re = /(login|register)/;
        let isAuth = re.test(this.props.location.pathname);
        return (
            <div>
                {isAuth ? undefined : <Header isLoggedIn={this.props.status.isLoggedIn} onLogout={this.handleLogout}/>}
                {this.props.children || <Home/>}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        status : state.authentication.status
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getStatusRequest : () => {
            return dispatch(getStatusRequest());
        },
        logoutRequest : () => {
            return dispatch(logoutRequest());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
//export default는 스크립트(js 파일)당 딱 하나
