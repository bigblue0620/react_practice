import React from 'react';
import {Authentication} from '../components';
import {connect} from 'react-redux';
import {loginRequest} from '../actions/authentication';

class Login extends React.Component
{
    constructor(props){
        super(props);
        this.handleLogin = this.handleLogin.bind(this);
    }

    handleLogin(id, pw)
    {
        //loginRequest를 하면 '../actions/authentication.js의 loginRequest가 실행되고 그 결과를 여기서 받음'
        return this.props.loginRequest(id, pw).then(
            () => {
                if(this.props.status == 'SUCCESS'){
                    let loginData = {isLoggedIn:true, username:id};
                    document.cookie = 'key=' + btoa(JSON.stringify(loginData));//btoa는 JavaScript의 base64 인코딩 함수

                    Materialize.toast('Webcome, ' + id + '!', 2000);//대화상자 표시
                    this.props.history.push('/');//라우팅을 트리거 (Link 를 누른것과 똑같은 효과)
                    return true;
                }else{
                    let $toastContent = $('<span style="color: #FFB4BA">Incorrect username or password</span>');
                    Materialize.toast($toastContent, 2000);//대화상자 표시
                    return false;
                }
            }
        );
    }

    render(){
        return (
            <div>
                <Authentication mode={true} onLogin={this.handleLogin}/>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        status : state.authentication.login.status
    }
};

const mapDisplatchToProps = (dispatch) => {
    return {
        loginRequest : (id, pw) => {
            return dispatch(loginRequest(id, pw));
        }
    };
};

export default connect(mapStateToProps, mapDisplatchToProps)(Login);
