import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Route, browserHistory, IndexRoute, Switch} from 'react-router-dom';
import {App, Home, Login, Register} from './containers/';

//redux
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import reducers from './reducers';
import thunk from 'redux-thunk';

const store = createStore(reducers, applyMiddleware(thunk));

const rootElement = document.getElementById('root');
ReactDOM.render(
    <Provider store={store}>
        <Router history={browserHistory}>
            <Switch>
                <Route exact path="/" component={App} />
                <Route path="/home" component={Home}/>
                <Route path="/login" component={Login}/>
                <Route path="/register" component={Register}/>
            </Switch>
        </Router>
    </Provider>,
    rootElement
);

/*
--save-dev is used to save the package for development purpose. Example: unit tests, minification..
--save is used to save the package required for the application to run.
*/
