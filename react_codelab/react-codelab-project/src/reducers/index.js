import authentication from './authentication';
import memo from './memo';
import {combineReducers} from 'redux';

export default combineReducers({authentication, memo});

//combineReducers 설명
//const counterApp = combineReducers({counter, extra});

/* 이 코드와 동일함.
const counterApp = ( state = { }, action ) => {
    return {
        counter: counter(state.counter, action),
        extra: extra(state.extra, action)
    }
}
 */

//combineReducers 는 여러개의 reducer를 한개로 합칠 때 사용 되는 redux 내장 메소드
//이 예제에서는 딱히 여러개의 reducer 를 사용 할 필요가 없었으므로, 예제로만 작성
//위의 reducer 를 사용하여 store를 만들게 되면, store 의 state 구조는 다음과 같이 생성
/*
{
    counter: { value: 0, diff: 1 }
    extra: { value: 'this_is_extra_reducer' }
}*/
