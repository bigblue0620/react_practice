var path = require('path');

module.exports = {
    resolve : {
        //root : path.resolve('./src'),
        extensions: ['', '.js', '.jsx']
        //modules: ['src', 'node_modules']
    },

    entry: ['./src/index.js', './src/style.css'],

    output: {
        path: __dirname + '/public/',
        filename: 'bundle.js'
    },

    module: {
        loaders: [
            {test : /\.js$/, loader : 'react-hot', exclude: /node_modules/},
            {test: /\.js$/, loader : 'babel?' + JSON.stringify({cacheDirectory: true, presets: ['es2015', 'react']}), exclude: /node_modules/},
            {test : /\.css$/, loader : 'style!css', exclude: /node_modules/}
        ]
    }
};
