import React from 'react';
import PhoneForm from './PhoneForm';
import PhoneInfoList from './PhoneInfoList';
import './App.css';

class App extends React.Component
{
    state = {
        information : [],
        keyword : ''
    };

    handleCreate = (data) => {
        const {information} = this.state;
        this.setState({
            information:information.concat({id:information.length, ...data})
        });
    };

    handleRemove = (id) => {
    	const {information} = this.state;
    	this.setState({
    		information:information.filter(info => info.id !== id)
    	});
    };

    handleUpdate = (id, data) => {
    	const {information} = this.state;
    	this.setState({
    		information:information.map((info) => (id === info.id) ? {...info, ...data} : info)
    	});
    };

    handleChange = (e) => {
    	this.setState({keyword:e.target.value});
    };

    render(){
    	console.log("App render ---------");
        const {information, keyword} = this.state;
        const filteredList = information.filter(
        	info => info.name.indexOf(keyword) !== -1
        	);

        return (
            <div className="App">
              <header className="App-header">
                <PhoneForm onCreate={this.handleCreate} />
              </header>
              <section>
                { /*information.length > 0 && JSON.stringify(information)*/}
                <PhoneInfoList data={filteredList} onRemove={this.handleRemove} onUpdate={this.handleUpdate} />
              </section>
              <br/>
              <footer>
              	<input placeholder="search name ..."
              			onChange={this.handleChange}
              			value={keyword} />
              </footer>
            </div>
      );
    }
}

// let state = {
//     information: [
//       {
//         id: 0,
//         name: '김민준',
//         phone: '010-0000-0000'
//       },
//       {
//         id: 1,
//         name: '홍길동',
//         phone: '010-0000-0001'
//       }
//     ]
// };

// let data = {id:0, name:'ccccc'};

// ((id, data) => {
// 	const {information} = state;
// 	let a = information.map((info) => (id === info.id)? {...info, ...data} : info);
// 	console.log(a);
// })(0, data);



// function App() {

//     const handleCreate = (data) => {
//         console.log(data);
//     };

//     return (
//         <div className="App">
//           <header className="App-header">
//             <PhoneForm onCreate={handleCreate} />
//           </header>
//           <nav>
//           </nav>
//         </div>
//   );
// }

export default App;
