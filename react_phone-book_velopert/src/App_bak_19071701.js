import React from 'react';
import logo from './logo.svg';
import Title from './Title';
import Counter from './Counter';
import './App.css';

function App() {
    // const name = 'react';
    // const style = {
    //     color:'red',
    //     fontSize:'calc(20px + 2vmin)'
    // };

    return (
        <div className="App">
          <header className="App-header">
            {/*<p>{name}</p>*/}
            <React.Fragment>
            	{/*<Title name="React" />*/}
            	<Title name='React' />
                { /*true && (<p style={style}>{name}</p>)*/ }
                {
                    /*(function(val){
                        if(val === 1) return <p>one</p>;
                        if(val === 2) return <p>Two</p>;
                        if(val === 3) return <p>Three</p>;
                    })(3)*/

                    /*((val) => {
                        if(val === 1) return <p>one</p>;
                        if(val === 2) return <p>Two</p>;
                        if(val === 3) return <p>Three</p>;
                    })(3)*/
                }
            </React.Fragment>
            <React.Fragment>
            	<Counter number={10} error={false} />
            </React.Fragment>
            <img src={logo} className="App-logo" alt="logo" />
            <p>
              Edit <code>src/App.js</code> and save to reload.
            </p>
            <a
              className="App-link"
              href="https://reactjs.org"
              target="_blank"
              rel="noopener noreferrer"
            >
              Learn React
            </a>
          </header>
        </div>
  );
}

export default App;
