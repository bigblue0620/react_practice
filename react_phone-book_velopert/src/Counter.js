import React from 'react';

class Counter extends React.Component
{
	state = {
		number : 0,
		error : false
	};

	constructor(props){
		super(props);
		//console.log(props);
		//console.log(this.state);
		this.state = {
			...props
		};
		console.log('constructor');
		console.log(this.state);
	}

	// UNSAFE_componentWillMount(){//deprecated
	// 	console.log('component Will Mount');
	// }

	componentDidMount(){
		console.log('component Did Mount');
		//외부 라이브러리 연동, 데이터 요청, DOM 관련 작업
	}
	
	//called whenever the new props, setState, and forceUpdate is being received.
	static getDerivedStateFromProps(nextProps, prevState){
		console.log('getDerivedStateFromProps');
		//console.log(nextProps);
		//console.log(prevState);
		//
		// if(nextProps.number !== prevState.number){
		// 	return {number:nextProps.number};
		// }

		return null;
	}

	shouldComponentUpdate(nextProps, nextState){
		console.log('shouldComponentUpdate');
		//console.log(nextProps);
		//console.log(nextState);
		if(nextState.number === 15) return false; //if 15 not re-render

		return true;
	}

	//componentWillUpdate(prevProps, prevState){//call when shouldComponentUpdate return true; //deprecated
	getSnapshotBeforeUpdate(prevProps, prevState){
		console.log('component Will Update');
		console.log('prevProps', prevProps);
		console.log('this.state', this.state);
		return {};
	}

	componentDidUpdate(prevProps, prevState, snapshot){
		console.log('component Did Update');
	}

	componentDidCatch(error, info){ //자식 컴포넌트에서 문제 발생시 캐치
		this.setState({error:true});
	}

	handleInc = () => {
		// this.setState({number:++this.state.number});
		this.setState(
			({number}) => ({
				number : ++number
			})
		);
	};

	handleDec = () => {
		let {number} = this.state;
		this.setState({number:--number});
	};

    render(){
    	console.log('render');

    	if(this.state.error) return (<h1>Error Happened ! </h1>);

        return (
            <div>
                <h1>Counter</h1>
                <div>value : {this.state.number}</div>
                {this.state.number === 4 && <ErrorMake />}
                <button onClick={this.handleInc}> + </button>
                <button onClick={this.handleDec}> - </button>
            </div>
        );
    }
}

const ErrorMake = () => {
	throw (new Error('Error Happened'));
	return(
	 	<div></div>
	);
};

export default Counter;