import React from 'react';

class PhoneForm extends React.Component
{
	state = {
		name : '',
		phone: ''
	};

	handleChange = (e) => {
		this.setState({[e.target.name] : e.target.value});
	};

	handleSubmit = (e) => {
		e.preventDefault(); //prevent page-reloading
		this.props.onCreate(this.state);
		this.setState({name:'', phone:''});
	}

	render(){    	
    	return (
            <form onSubmit={this.handleSubmit}>
            	<input
            		name='name'
            		placeholder='name'
            		value={this.state.name}
            		onChange={this.handleChange} />
            	{' '}
            	<input
            		name='phone'
            		placeholder='phone number'
            		value={this.state.phone}
            		onChange={this.handleChange} />
            	{/*<div>{this.state.name}  {this.state.phone}</div>*/}
            	{' '}
            	<button type='submit'>submit</button>
            </form>
        );
    }
}

export default PhoneForm;