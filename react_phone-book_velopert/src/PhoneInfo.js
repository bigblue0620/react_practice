import React from 'react';

/*function PhoneInfo()
{
    static defaultProps = {
        info : {
            id : 0,
            name : '',
            phone : '000-0000-0000'
        }
    };

    const style = {
        border: '1px solid black',
        padding : '8px',
        margin : '8px'
    };

    const {name, phone, id} = props.info;

    return (
        <div style={style}>
            <div>{name}</div>
            <div>{phone}</div>
        </div>
    );
}*/

// const PhoneInfo = (props) => {
//     const style = {
//         border: '1px solid black',
//         padding : '8px',
//         margin : '8px'
//     };

//     const {name, phone} = props.info;

//     const handleRemove = () => {
//         const {info, onRemove} = props;
//         onRemove(info.id);
//     };

//     return (
//         <div style={style}>
//             <div>{name}</div>
//             <div>{phone}</div>
//             <button onClick={handleRemove}>delete</button>
//         </div>
//     );
// };

class PhoneInfo extends React.Component
{
    state = {
        editing : false,
        name : '',
        phone : ''
    };

    handleRemove = () => {
        const {info, onRemove} = this.props;
        onRemove(info.id);
    };

    handleToggleEdit = () => {
        console.log("----handleToggleEdit----");
        const {editing} = this.state;
        this.setState({editing:!editing});
    };

    handleChange = (e) => {
        const {name, value} = e.target;
        this.setState({
            [name]:value
        });
    };

    shouldComponentUpdate(nextProps, nextState)
    {
        if(!this.state.editing && !nextState.editing && nextProps.info === this.props.info){
            return false;
        }

        return true;
    }

    componentDidUpdate(prevProps, prevState)
    {
        console.log("----componentDidUpdate----");
        const {info, onUpdate} = this.props;

        if(!prevState.editing && this.state.editing){
            this.setState({name:info.name, phone:info.phone});
        }

        if(prevState.editing && !this.state.editing){
            onUpdate(info.id, {name:this.state.name, phone:this.state.phone});
        }
    };

    render(){

        console.log('render Phone item ----', this.props.info.id);


        const style = {
            border: '1px solid black',
            padding : '8px',
            margin : '8px'
        };

        const {editing} = this.state;

        if(editing){
            return (
                <div style={style}>
                    <div>
                        <input value={this.state.name} 
                                name = 'name'
                                placeholder='name'
                                onChange={this.handleChange} />
                    </div>
                    <div>
                        <input value={this.state.phone} 
                                name = 'phone'
                                placeholder='phone'
                                onChange={this.handleChange} />
                    </div>
                    <button onClick={this.handleToggleEdit}>apply</button>
                    <button onClick={this.handleRemove}>delete</button>
                </div>
            );
        }

        const {name, phone} = this.props.info;

        return(
            <div style={style}>
                <div>{name}</div>
                <div>{phone}</div>
                <button onClick={this.handleToggleEdit}>modify</button>
                <button onClick={this.handleRemove}>delete</button>
            </div>
        );
    };
};

PhoneInfo.defaultProps = {
    info : {
        id : 0,
        name : '',
        phone : '000-0000-0000'
    }
};

export default PhoneInfo;