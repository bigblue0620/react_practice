import React from 'react';
import PhoneInfo from './PhoneInfo';

/*function PhoneInfoList(props={data:[]})
{
    const {data} = props;
    const list = data.map((info) => (<PhoneInfo key={info.id} info={info} />));

    return (
        <div>
            {list}
        </div>
    );
}*/

/*const PhoneInfoList = (props) => 
{
    const {data, onRemove, onUpdate} = props;
    const list = data.map(
        (info) => (<PhoneInfo key={info.id} 
                                info={info} 
                                onRemove={onRemove} 
                                onUpdate={onUpdate}  />));

    //console.log('render PhoneInfoList');
    return (
        <div>
            {list}
        </div>
    );
}*/

class PhoneInfoList extends React.Component
{
    shouldComponentUpdate(nextProps, nextState) {
        //console.log(JSON.stringify(this.props));
        //console.log(JSON.stringify(nextProps));
        return nextProps.data !== this.props.data;
    }
  
    render() {
        console.log('render PhoneInfoList');
        const { data, onRemove, onUpdate } = this.props;
        const list = data.map(
            info => (
                <PhoneInfo
                    key={info.id}
                    info={info}
                    onRemove={onRemove}
                    onUpdate={onUpdate}
                />)
            );

    return (
      <div>
        {list}    
      </div>
    );
  }
};

PhoneInfoList.defaultProps = {
    data:[],
    onRemove:() => console.warn('onRemove not defined'),
    onUpdate:() => console.warn('onUpdate not defined')
}

export default PhoneInfoList;