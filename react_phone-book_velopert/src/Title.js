import React from 'react';

// class Title extends React.Component
// {
//     // static defaultProps = {
//     //     name : 'Default Name'
//     // };

//     render(){
//         return (
//             <div>
//                 Hello. My name is {this.props.name}
//             </div>
//         );
//     }
// }

const Title = ({name}) => {
    return (
        <div>
            Hello. My name is {name}
        </div>
    );
}

Title.defaultProps = {
    name : 'Smile'
}

export default Title;