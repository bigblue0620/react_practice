import React from 'react';
import Keypad from './Keypad';

class InputForm extends React.Component
{
	state = {
		num : 0,
        showKeypad : false
	};

	handleChange = (e) => {
	   this.setState({[e.target.name] : e.target.value});
	};

	// handleSubmit = (e) => {
	// 	e.preventDefault(); //prevent page-reloading
	// 	this.props.onCreate(this.state);
	// 	this.setState({name:'', phone:''});
	// }

    handleClick = (e) => {
        this.setState({showKeypad : !this.state.showKeypad});
    };

    handlePlus = (isPlus) => {
        
    };

	render(){    	
    	return (
            <div className='inputForm'>
                <input name='num'
                		placeholder=' '
                		value={this.state.num}
                        onChange={this.handleChange}
                		onClick={this.handleClick} />
                
                <button onClick={()=>this.handlePlus(false)}>-</button>
                <button onClick={()=>this.handlePlus(true)}>+</button>
                {' '}{this.props.min}~{this.props.max}
                <br/>
                {this.state.showKeypad ? <Keypad /> : null}
            </div>
        );
    }
};

InputForm.defaultProps = {
    min : 0,
    max : 999
};

export default InputForm;