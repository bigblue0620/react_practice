import React from 'react';
import { RouterOutlet } from 'react-router-outlet';
import routes from './helper/Routes';
import { Router } from "react-router";
import { createBrowserHistory } from "history";
import { Provider } from 'react-redux';
import reducers from './redux/reducer';
import createStore from './redux/store';

function App()
{
    const history = createBrowserHistory();
    const store = createStore(reducers);

    return (
        <Router history={history}>
            <Provider store={store}>
                <div>
                    <RouterOutlet routes={routes} />
                </div>
            </Provider>
        </Router>
    );
}

export default App;