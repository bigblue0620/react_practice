import React, { ChangeEvent, FocusEvent, Component } from 'react';
import styled from "styled-components";
import { IInputProps, IInputState } from '../helper/index';

class StyledInputElement extends Component<IInputProps, IInputState>
{
    constructor(props: IInputProps){
        super(props);
        this.state = {autofocus: this.props.autofocus, value: this.props.value};
    }

    onChangeProc = (e: ChangeEvent<HTMLInputElement>) => {
        this.setState({autofocus:true, value:e.target.value});
        // this.props.onChange?.(e.target.value);
        this.props.action?.(e.target.value);
    };

    onBlurProc = (e: FocusEvent) => {
        this.setState({autofocus:false, value:(e.target as HTMLInputElement).value});
    }

    render() {
        const Element = styled.input`${this.props.style}`;
        return (
            <Element type={this.props.type} placeholder={this.props.placeholder} onBlur={this.onBlurProc} onChange={this.onChangeProc} autoFocus={this.state.autofocus} value={this.state.value} />
        );
    }
}

export default StyledInputElement;