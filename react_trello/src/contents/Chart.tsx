import React from 'react';
import { Doughnut, Line } from "react-chartjs-2";
//import Chart from "chart.js";

import 'chartjs-plugin-streaming';
import './Chart.scss';

function Chart()
{
    const doughhnutData = {
        labels: ["긍정적", "부정적", "보통"],
        datasets: [
          {
            labels: ["긍정적", "부정적", "보통"],
            data: [60, 13, 27],
            borderWidth: 2,
            hoverBorderWidth: 3,
            backgroundColor: [
              "rgba(238, 102, 121, 1)",
              "rgba(98, 181, 229, 1)",
              "rgba(255, 198, 0, 1)"
            ],
            fill: true
          }
        ]
    };

    //const lineChart = createRef();

    const randomScalingFactor = () => {
        return (Math.random() > 0.5 ? 1.0 : -1.0) * Math.round(Math.random() * 100);
    }

    // const Fetch = async (url:string, options:any={}) => {
    //     const response = await fetch(url, options);
    //     const body = await response.json();
    //     if (response.status !== 200) throw Error(body.message);
    //     return body;
    // };

    // const [ tmp, setTmp ] = useState([{x: Date.now(), y: randomScalingFactor()}]);

    const onRefresh = (chart: any) => {
        // Fetch(`/board/tasklist/${this.props.id}/taskitem`).then(res => {
        //     const tmp = res;
        //     this.setState({taskList:this.state.taskList.concat(tmp)});
        // }).catch(err => console.log('get tasklist error', err));

        // chart.data.datasets.forEach((dataset: any) => {
        //     dataset.data.push({
        //         x: Date.now(),
        //         y: randomScalingFactor()
        //     });
        // });

        chart.config.data.datasets.forEach(function(dataset: any) {
            dataset.data.push({
                x: Date.now(),
                y: randomScalingFactor()
            });
        });

        chart.update();

        // chart.update({
        //     preservation: true
        // });

        // //@ts-ignore
        // lineChart.current?.chartInstance.data.datasets[0].data.push({ 
        //     x: Date.now(), y: randomScalingFactor()
        // });

        // //@ts-ignore
        // lineChart.current?.chartInstance.update({
        //     preservation: true,
        // });

        //setTmp(tmp.concat({x: Date.now(), y: randomScalingFactor()}));
    }

    const LineData = {
        labels: [],
        datasets: [{
            label: 'Dataset 1 (linear interpolation)',
            borderColor: 'rgb(255, 99, 132)',
            borderWidth: 1,
            fill: false,
            data: []
        }]
    }

    const LineOptions = {
        responsive: true,
        scales: {
            xAxes: [{
                type: "realtime",
                realtime: {
                    duration: 20000,
                    ttl: 60000,
                    refresh: 1000,
                    delay: 2000,
                    onRefresh: onRefresh
                }
            }],
            yAxes: [{
                type: 'linear',
                scaleLabel: {
                    display: true,
                    labelString: 'value'
                }
            }]
        },
        // plugins: {
        //     streaming: {            // per-chart option
        //         frameRate: 30       // chart is drawn 30 times every second
        //     }
        // }
    }

    return (
        <div>
            <div className="wrapper">
                <Doughnut
                    options={{
                        responsive: false,
                        legend: {
                            display: true,
                            position: "bottom",
                            labels : {
                                boxWidth: 20
                            }
                        }
                    }}
                    data={doughhnutData} width={250} height={250}/>
            </div>
            <div className="wrapper">
                <Line data={LineData} 
                    options={LineOptions} 
                    //@ts-ignore
                    width={450} height={250} /*ref={lineChart}*/ />
            </div>
        </div>
    );
}

export default Chart;