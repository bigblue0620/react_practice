import React, { useEffect, createRef, useRef } from 'react';
import ChartComponent, { Line, LinearComponentProps } from "react-chartjs-2";
import 'chartjs-plugin-streaming';
import { ChartPoint } from 'chart.js';
//import { Fetch } from '../helper/utils';

function Chart2()
{
    //let interval: any = null;
    let socket = useRef<WebSocket | null>(null);
    const lineChart = createRef<ChartComponent<LinearComponentProps>>();
    
    useEffect(() => {   //exec componetnDidMount,, componentDidUpdate
        // clearInterval(interval);
        // interval = setInterval(() => {
        //     Fetch(`/test/random`).then(res => {
        //         //@ts-ignore
        //         lineChart.current?.chartInstance.data.datasets[0].data.push({ 
        //             x: Date.now(),
        //             y: res
        //         });
        //         //@ts-ignore
        //         lineChart.current?.chartInstance.update();
        //     }).catch(err => console.log('get data from server error', err));
        // }, 1000);

        socket.current = new WebSocket("ws://localhost:9000");
        socket.current.onmessage = (evt: any) => {
            //lineChart.current?.chartInstance.data.datasets[0].data.push({x: Date.now(), y: Number(evt.data)});
            lineChart.current?.chartInstance.data.datasets?.forEach((dataset) => {
                (dataset.data as ChartPoint[])?.push({x: Date.now(), y: Number(evt.data)});
            });
            lineChart.current?.chartInstance.update();
        }

        return () => {  //exec componentWillUnmount
            console.log("destroy socket close");
            socket.current?.close();
        };

    }, [lineChart]);

    // useEffect(() => {
    //     return () => {
    //         console.log("destroy interval clear");
    //         clearInterval(interval);
    //     };
    // }, [interval]);

    // const onRefresh = (chart: any) => {
    //     console.log('2', value);
    //     chart.data.datasets[0].data.push({
    //         x: Date.now(),
    //         y: value
    //     });

    //     chart.update();
    //     //console.log('2', value);
    // }

    const LineData = {
        labels: [],
        datasets: [{
            label: 'Dataset 1 (linear interpolation)',
            borderColor: 'rgb(255, 99, 132)',
            backgroundColor: 'rgb(255, 255, 255)',
            borderWidth: 1,
            fill: false,
            data: []
        }]
    }

    const LineOptions = {
        responsive: true,
        scales: {
            xAxes: [{
                type: "realtime",
                realtime: {
                    duration: 20000,
                    ttl: 60000,
                    refresh: 1500,
                    delay: 2000,
                    //onRefresh: onRefresh
                }
            }]
        }
    }

    return (
        <div>
            <div className="wrapper">
                {/* @ts-ignore */}
                <Line data={LineData} options={LineOptions} width={450} height={250} ref={lineChart} />
            </div>
        </div>
    );
}

export default Chart2;




//수동으로 차트에 addData(label도 추가)해서 차트 업데이트 하는 소스
// class Chart2 extends Component<any, any>
// {
//     componentDidMount(){
//         var c = document.getElementById('test');
//         //@ts-ignore
//         var ctx = c.getContext('2d');

//         const LineData = {
//             datasets: [{
//                 label: 'Dataset 1 (linear interpolation)',
//                 borderColor: 'rgb(255, 99, 132)',
//                 borderWidth: 1,
//                 fill: false,
//                 data: []
//             }]
//         }

//         const LineOptions = {
//             responsive: false,
//             scales: {
//                 xAxes: [{
//                     type: "time",
//                     distribution: 'series'
//                 }],
//                 yAxes: [{
//                     scaleLabel: {
//                         display: true,
//                         labelString: 'value'
//                     }
//                 }]
//             }
//         }

//         const data = {
//             type: 'line',
//             data: LineData,
//             options: LineOptions
//         }

//         var chart = new Chart(ctx, {
//             type: 'line',
//             data: {
//                 labels: [],
//                 datasets: [{
//                     label: '# of Votes',
//                     data: [1],
//                     fill: false,
//                     borderWidth: 1
//                 }]
//             },
//             options: {
//                 responsive: false,
//                 scales: {
//                     xAxes: [{
//                         // type: 'time',
//                         // time: {
//                         //     tooltipFormat: "hh:mm:ss",
//                         //     displayFormats: {
//                         //       hour: 'MMM D, hh:mm:ss'
//                         //     }
//                         // }
//                         // ticks: {
//                         //     source: 'data'
//                         // }
//                     }]
//                 }
//             }
//         });

//         setInterval(function() {
//             //@ts-ignore
//             chart.data.labels.push(new Date().toTimeString().slice(0,8));
//             //@ts-ignore
//             chart.data.datasets[0].data.push(Math.random() * 100);
//             chart.update();
//         }, 1000);

//         // //@ts-ignore
//         // var dataset = chart.data.datasets[0];
//         // //@ts-ignore
//         // chart.data.labels.push('a');
//         // //@ts-ignore
//         // dataset.data.push(10);
//         // //@ts-ignore
//         // chart.data.labels.push('b');
//         // //@ts-ignore
//         // dataset.data.push(25);
//         // chart.update();
//         // //@ts-ignore
// 		// // for(var i=0; i<dataset.length; i++){
// 		// // 	console.log(dataset);
//         // //     //데이터 갯수 만큼 반복
//         // //     //@ts-ignore
//         // //     const data = dataset[i].data;
//         // //     //@ts-ignore
// 		// // 	for(var j=0 ; j < data.length ; j++){
//         // //         //@ts-ignore
// 		// // 		data[j] = Math.floor(Math.random() * 50);
//         // //     }
            
//         // //     chart.update();
// 		// // }
//     }

//     render(){
//         return (
//             <div>
//                 <canvas id='test' width="450px" height="300px"></canvas>
//             </div>
//         );
//     }
// }

// export default Chart2;