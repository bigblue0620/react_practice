import React, { MouseEvent, FocusEvent } from 'react';
import TaskContainer from './TaskContainer';
import './Dashboard.scss';
import { Fetch } from '../helper/utils';
import { connect } from 'react-redux';
import * as actions from '../redux/action/tasklist';
import { IDashboardState } from '../helper/index';
import { IReduxState } from '../redux/reducer';
import { ITaskListItem, ITaskList } from '../redux/reducer/tasklist';
import { Dispatch, Action } from 'redux';

interface IProps extends ReturnType<typeof mapStateToProps>, ReturnType<typeof mapDispatchToProps>{}

class Dashboard extends React.Component<IProps, IDashboardState>
{
    constructor(props: IProps){
        super(props);
        this.state = {showAddContainerForm: false};
    }

    componentDidMount(){
        Fetch('/board/tasklist').then(res => {
            const tmp = res;
            this.props.initTaskList(tmp);
        }).catch(err => console.log('get tasklist error', err));
    }

    clickAddContainer = () => {
        this.setState({showAddContainerForm: true});
    }

    hideAddContainerForm = (e: FocusEvent | MouseEvent<HTMLSpanElement>) =>
    {
        //컨테이너 입력폼의 add 버튼의 경우 blur 이벤트 처리 제외
        const relatedTarget = (e as unknown as FocusEvent).relatedTarget as HTMLButtonElement;
        if(relatedTarget && relatedTarget.tagName === 'BUTTON' && relatedTarget.value === 'AddContainer'){
            return;
        }

        this.setState({showAddContainerForm: false});
    }

    addContainerForm = (e: MouseEvent<HTMLButtonElement>) =>
    {
        e.stopPropagation();
        const value = (e.currentTarget.previousElementSibling as HTMLInputElement).value;
        if(value){
            //_id 값은 nodejs (backend 측)에서 생성함
            this.setState({showAddContainerForm: false});
            Fetch('/board/tasklist/', {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({_id: '', name: value, enable: true})})
            .then(res => {
                this.props.addTaskList?.({_id: res._id, name: value, enable: true});
            }).catch(err => console.log('modify TaskContainer Name error', err));
        }
    }

    render() {
        const { list } = this.props;

        return (
            <div>
                {list!.map((el: ITaskListItem) => <TaskContainer key={el._id} id={el._id} name={el.name}></TaskContainer>)}
                <div className="add-container" onClick={this.clickAddContainer} onBlur={this.hideAddContainerForm}>
                    {(this.state.showAddContainerForm === false ? <span>&#65291; Add another list</span> 
                    :   <div className='input'>
                            <input type='text' placeholder='Enter Name ...' autoFocus></input>
                            <button onClick={this.addContainerForm} value='AddContainer'>Add</button><span onMouseDown={this.hideAddContainerForm}></span>
                        </div>)}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state: IReduxState) => ({
    list: state.tasklist?.list,
});

const mapDispatchToProps = (dispatch: Dispatch<Action>) => ({
    initTaskList: (obj: ITaskList) => dispatch(actions.initTaskList(obj)),
    addTaskList: (obj: ITaskListItem) => dispatch(actions.addTaskList(obj)) //MEMO delete는 TaskContainer.tsx 에서 실시하므로 redux 적용
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);