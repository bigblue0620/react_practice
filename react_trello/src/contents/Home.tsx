import React from 'react';
import { Link } from 'react-router-dom';

function Home() {
    return (
        <div>
            <Link to="/login">로그인</Link>
            <br></br>
            <Link to="/dashboard">대시보드</Link>
            <br></br>
            <Link to="/spread">스프레드시트</Link>
            <br></br>
            <Link to="/chart">차트</Link>
            <br></br>
            <Link to="/chart2">차트2 (http polling, web socket)</Link>
        </div>
    );
}

export default Home;