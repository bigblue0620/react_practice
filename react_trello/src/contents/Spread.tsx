import React, { useMemo } from 'react';
import { useTable, useSortBy } from 'react-table';
import './Spread.scss';
import makeData from '../helper/makeData';
//import { originData } from '../helper/makeData';

function Spread()
{
    // const data = [
    //     {
    //         col1: 'Hello',
    //         col2: 'World',
    //     },
    //     {
    //         col1: 'react-table',
    //         col2: 'rocks',
    //     },
    //     {
    //         col1: 'whatever',
    //         col2: 'you want',
    //     },
    // ]

    // const columns:Column<{ col1: string; col2: string; }>[] = useMemo(
    //     () => [
    //       {
    //         Header: 'A',
    //         accessor: 'col1', // accessor is the "key" in the data
    //       },
    //       {
    //         Header: 'B',
    //         accessor: 'col2',
    //       },
    //     ],
    //     []
    // );

    const data = useMemo(() => makeData(50), []);

    const columns = useMemo(
        () => [
            {
                Header: 'Name',
                columns: [
                    {
                        Header: 'First Name',
                        accessor: 'firstName',
                    },
                    {
                        Header: 'Last Name',
                        accessor: 'lastName',
                    },
                ],
            },
            {
                Header: 'Info',
                columns: [
                    {
                        Header: 'Age',
                        accessor: 'age',
                        sortType: 'basic'
                    },
                    {
                        Header: 'Visits',
                        accessor: 'visits',
                        sortType: 'basic'
                    },
                    {
                        Header: 'Status',
                        accessor: 'status',
                        sortType: 'basic'
                    },
                    {
                        Header: 'Profile Progress',
                        accessor: 'progress',
                        sortType: 'basic'
                    },
                ]
            }],
        []
    );

    let {
        headerGroups,
        rows,
        prepareRow,
    } = useTable({ columns, data }, useSortBy);

    const doSort = (column: any) => {
        console.log(column);
        console.log(column.isSorted);
        console.log(!column.isSortedDesc);

        //------------------------------------------------
        //        isSorted    isSortedDesc
        //------------------------------------------------
        //          false       true            ASC
        //------------------------------------------------
        //          true        true            DESC
        //------------------------------------------------
        //          true        false           NULL
        //------------------------------------------------
    };

    return (
        <div className='wrapper'>
            <table>
                <thead>
                    {headerGroups.map(headerGroup => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            <th></th>
                            { headerGroup.headers.map(column => {
                                // @ts-ignore
                                const { colSpan } = column.getHeaderProps();
                                // @ts-ignore
                                const { isSorted, ...prop } = column.getSortByToggleProps({isSorted: true});
                                return (<th {...column.getHeaderProps(prop)}>
                                    {column.render('Header')}
                                    {/* @ts-ignore */}
                                    { colSpan === 1 ? <span onClick={() => doSort(column, column.isSorted, !column.isSortedDesc)}>{column.isSorted ? column.isSortedDesc ? '🔽': '🔼' : String.fromCharCode(8645)}
                                                    </span> : null }
                                </th>);
                            })}
                        </tr>
                    ))}
                </thead>
                <tbody>
                    {rows.map((row, i) => {
                        prepareRow(row);
                        return (
                            <tr key={'tr_'+i}>
                                <td>{i+1}</td>
                                {row.cells.map((cell, i) => (
                                    <td key={'td_'+i}>
                                        {cell.render('Cell')}
                                    </td>
                                ))}
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default Spread;