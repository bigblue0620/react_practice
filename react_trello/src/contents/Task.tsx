import React, { Fragment, useState, DragEvent, MouseEvent } from 'react';
import styled from "styled-components";
import { TaskStyleStr, TaskSubjectStr, TaskBtnStr } from '../helper/styled-resource';
import { EventEmitter, Fetch } from '../helper/utils';
import TaskModifyModalPopup from './task.modify.modal.popup';
import { ITaskProps } from '../helper/index';
import { useSelector } from 'react-redux';
import { ITaskListItem } from '../redux/reducer/tasklist';

function Task(props: ITaskProps)
{
    const TaskItem = styled.div`${TaskStyleStr}`;
    const Subject = styled.span`${TaskSubjectStr}`;
    const Button = styled.button`${TaskBtnStr}`;
    const [subject, updateTaskName] = useState(props.subject);
    const [isOpenModal, toggleOpenModal] = useState(false);
    const [isShowTaskList, toggleShowTaskList] = useState(false);
    const [offset, setOffset] = useState({top: 0, left: 0});
    // @ts-ignore
    const taskList: ITaskListItem[] = useSelector((state) => state.tasklist.list);
    
    let dragItem: HTMLDivElement | null;
    let isDragging: boolean = false;
    
    const dragStart = (e: DragEvent) => {
        dragItem = e.target as HTMLDivElement;
        e.dataTransfer.effectAllowed = "move";
        isDragging = false;
        e.dataTransfer?.setData('text/plain', JSON.stringify({id:props.id, subject:props.subject, pId:props.pId}));//MEMO can't use dataTransfer in chrome except in the drop event 
        // EventEmitter.dispatch('updateDraggingInfo', {id:props.id, pId:props.pId, h:dragItem.offsetHeight});
        EventEmitter.dispatch('updateDraggingInfo', {id:props.id, pId:props.pId, h:dragItem.offsetHeight + 10});
    };

    const drag = (e: DragEvent) => {
        if(!isDragging){
            dragItem!.style.display = 'none';
            // dragItem.style.boxShadow = 'none';
            // dragItem.style.backgroundColor = '#dedede';
            // dragItem.firstElementChild.style.color = '#dedede';
            isDragging = true;
        }
    };

    const dragEnd = () => {
        console.log('-------------- drag end');
        //EventEmitter.dispatch('deletePlaceholder', null); //drag end - > drop 순으로 이벤트가 발생할 경우 place holder 를 지우면 안되므로 주석 처리
        //dragItem!.style.display = 'block';
        isDragging = false;
        // dragItem.style.boxShadow = '0 1px 0 rgba(9,30,66,.25)';
        // dragItem.style.backgroundColor = '#fff';
        // dragItem.firstElementChild.style.color = '#172b4d';
        dragItem = null;
    };

    const modify = (e: MouseEvent) => {
        const pos = (e.currentTarget.parentNode as Element).getClientRects()[0];
        setOffset({top: pos.y, left: pos.x});
        toggleOpenModal(true);
    };

    const toggleModal = () => {
        toggleOpenModal(false);
        toggleShowTaskList(false);
    }

    const modifySubject = (e: MouseEvent) => {
        const value = (e.currentTarget.previousSibling as HTMLTextAreaElement).value;

        Fetch(`/board/tasklist/${props.pId}/taskitem/${props.id}`, {method: 'PATCH', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({_id:props.id, key:'subject', value})})
            .then(() => {
                updateTaskName(value);
                toggleModal();
            }).catch(err => console.log('modify Task Subject error', err));
    };

    const toggleTaskList = () => {
        toggleShowTaskList(!isShowTaskList);
    }

    const deleteTask = () => {
        Fetch(`/board/tasklist/${props.pId}/taskitem/${props.id}`, {method: 'DELETE', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({_id:props.id})})
            .then(() => {
                toggleModal();
                EventEmitter.dispatch('updateTaskList', {id:props.id, opId:props.pId, info:null});
            }).catch(err => console.log('modify Task Subject error', err));
    }

    const moveTask = (e: MouseEvent) => {
        const newPId = (e.target as HTMLSpanElement).id;
        const oldPId = props.pId;

        const body = {_id: props.id, key: '_pid', value: newPId};
        Fetch(`/board/tasklist/${props.pId}/taskitem/${props.id}`, {method: 'PATCH', headers: {'Content-Type': 'application/json'}, body: JSON.stringify(body)})
            .then(() => {
                EventEmitter.dispatch('updateTaskList', {id:props.id, opId:oldPId, npId:newPId, info:{_id: props.id, _pId: newPId, subject: props.subject, appendLast:true}});
                toggleShowTaskList(!isShowTaskList);
                toggleModal();
            }).catch(err => console.log('modify Task Subject error', err));
    }

    return (
        <Fragment>
            <TaskItem id={props.id} draggable="true" onDragStart={dragStart} onDrag={drag} onDragEnd={dragEnd}>
                <Subject>{subject}</Subject>
                {/* <Button onClick={(e) => {toggleModal(true)}>&#10000;</Button> */}
                <Button onClick={modify}>&#10000;</Button>
            </TaskItem>
            <TaskModifyModalPopup isOpen={isOpenModal} /*onAfterOpen={afterOpenModal}*/ onRequestClose={toggleModal} offset={offset}>
                <div className='task-modify-modal-popup'>
                    <div>
                        <textarea defaultValue={props.subject}></textarea>
                        <button onClick={modifySubject}>Save</button>
                    </div>
                    <div>
                        <button onClick={toggleTaskList}>Move</button>
                        <button onClick={deleteTask}>Delete</button>
                    </div>
                    {isShowTaskList &&
                        <div>
                            <span onClick={() => toggleShowTaskList(false)}>x</span>
                            {taskList.map((el: ITaskListItem) => <span onClick={moveTask} id={el._id}>{el.name}</span>)}
                        </div>
                    }
                    <div>

                    </div>
                </div>
            </TaskModifyModalPopup>
        </Fragment>
    );
}

export default Task;