import React, { RefObject, Component, FocusEvent, DragEvent, KeyboardEvent, MouseEvent } from 'react';
import './TaskContainer.scss';
import Task from './Task';
import { EventEmitter } from '../helper/utils';
import Popover from '@material-ui/core/Popover';
import { Fetch } from '../helper/utils';
import { ITask, ITaskContainerState, ITaskContainerProps, ITaskProps, IDynamicData } from '../helper/index';
import * as actions from '../redux/action/tasklist';
import { connect } from 'react-redux';

const placeholder = document.createElement("div"); placeholder.className = "placeholder";

class TaskContainer extends Component<ITaskContainerProps, ITaskContainerState>
{
    addTaskContainerRef: RefObject<HTMLDivElement>;
    addTaskTextAreaRef: RefObject<HTMLTextAreaElement>;
    containerRef: RefObject<HTMLDivElement>;
    draggingInfo: ITaskProps | null = null;

    constructor(props: ITaskContainerProps)
    {
        super(props);

        this.state = {nameEditing: false, addTask: false, taskList: [], anchorEl: null};
        this.addTaskContainerRef = React.createRef();
        this.addTaskTextAreaRef = React.createRef();
        this.containerRef = React.createRef();

        EventEmitter.subscribe('updateTaskList', (data: any) => this.updateTaskList(data));
        EventEmitter.subscribe('updateDraggingInfo', (data: any) => this.updateDraggingInfo(data));
        EventEmitter.subscribe('deletePlaceholder', () => this.deletePlaceholder());
    }

    componentDidMount()
    {
        Fetch(`/board/tasklist/${this.props.id}/taskitem`).then(res => {
            this.setState({taskList:this.state.taskList.concat(res)});
        }).catch(err => console.log('get tasklist error', err));

        this.addTaskTextAreaRef?.current?.focus();
    }

    modifyTaskContainerName = (e: FocusEvent) => {
        const value = (e.target as HTMLTextAreaElement).value;
        if(!value){
            return;
        }

        Fetch(`/board/tasklist/${this.props.id}`, {method: 'PATCH', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({_id:this.props.id, name:value})})
            .then(res => {
                this.setState({nameEditing: false});
            }).catch(err => console.log('modify TaskContainer Name error', err));
    }

    addTask = (e: FocusEvent) => {
        const target = e.target as HTMLTextAreaElement;
        const value = target.value;
        target.value = '';
        const isFirstIn = (this.addTaskContainerRef.current?.previousElementSibling == null);

        if(value){
            const body = {_id:'', _pid: this.props.id, subject: value, order: (isFirstIn) ? 1 : this.state.taskList.length + 1};
            Fetch(`/board/tasklist/${this.props.id}/taskitem`, {method: 'POST', headers: {'Content-Type': 'application/json'}, body: JSON.stringify(body)})
                .then(res => {
                    const tmp = [{_id: res._id, _pid: this.props.id, subject: value, order: (isFirstIn) ? 1 : this.state.taskList.length + 1}];
                    if(isFirstIn){
                        this.setState({taskList: tmp.concat(this.state.taskList as []), addTask: false}, () => {
                            EventEmitter.dispatch('updateTaskList', {id:'', npId: this.props.id, opId: this.props.id, info: null});
                        });
                    }else{
                        this.setState({taskList: this.state.taskList.concat(tmp), addTask: false});
                    }
                }).catch(err => console.log('add taskitem error', err));
        }else{
            this.setState({addTask: false});
        }
    }

    cancelAddTask = () => {
        this.setState({addTask: false});
        this.addTaskTextAreaRef!.current!.value = '';
    }

    dragOver = (e: DragEvent) => {
        e.preventDefault();
        const target = e.target as HTMLElement;
        const currentTarget = e.currentTarget as HTMLElement;

        if(target.classList.contains('placeholder')){
            return;
        }

        //console.log(this.draggingInfo);
        placeholder.style.height = `${this.draggingInfo?.h}px`;
        if(target.id && (target.id !== currentTarget.id)){ //테스크 아이템에 드래그중인 아이템이 over 되었을 경우
            //console.log("over -- item : ", e.target.id);
            currentTarget.insertBefore(placeholder, target);
        }
    }

    mouseOverFooter = (e: MouseEvent) => {
        if(!this.draggingInfo){
            e.currentTarget.classList.add("hover");
        }
    }

    mouseOutFooter = (e: MouseEvent) => {
        e.currentTarget.classList.remove("hover");
    }

    dragOverFooter = (e: DragEvent) => { //드래그하는 태스크 아이템이 footer 영역상에 있을 때 container에 placeholder insert
        //console.log("over -- footer");
        e.preventDefault();
        placeholder.style.height = `${this.draggingInfo?.h}px`;
        this.containerRef.current?.insertBefore(placeholder, this.containerRef.current!.lastChild);
        //e.currentTarget.firstChild.classList.remove('hover');
    }

    dropProc = (e: DragEvent) => {
        console.log('---- drop container');
        const info = JSON.parse(e.dataTransfer.getData('text/plain'));
        const pId = e.currentTarget.id;

        if(!info || !pId) {
            this.deletePlaceholder();
            return;
        }

        Fetch(`/board/tasklist/${this.props.id}/taskitem/${info.id}`, {method: 'PATCH', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({_id: info.id, key: '_pid', value: pId})})
            .then(() => {
                //npId :  new parent id, opId: old parent id
                EventEmitter.dispatch('updateTaskList', {id:info.id, npId: pId, opId: info.pId, info: {_id: info.id, _pId: pId, subject: info.subject}});
            })
            .catch(err => console.log('update taskitem _pid error', err));
    }

    updateDraggingInfo = (data: ITaskProps) => {
        this.draggingInfo = data;
    }

    updateTaskList = (data: IDynamicData) => 
    {
        this.draggingInfo = null;
        const {id, npId, opId, info} = data;
        const tmp = this.state.taskList.filter((el) => el._id !== id);
        let arr: ITask [] = [];

        if(this.props.id === npId && info){
            //만약 placeholder가 마지막 엘리먼트(addTask) 앞에 있다면 taskList 마지막에 추가
            const ch = this.containerRef.current?.children || [];
            const len = ch?.length || 0;

            let appendLast = (ch[len-2] && (ch[len-2]).isEqualNode(placeholder)) ? true : false;
            if(!appendLast && typeof info.appendLast != 'undefined'){
                appendLast = info.appendLast;
            }

            if(appendLast){
                arr = tmp;
                arr = arr.concat([info]);
            }else{
                let idx = -1;
                for(let i=0; i<len; i++){
                    if((ch[i]).isEqualNode(placeholder)){
                        idx = i;
                        break;
                    }
                }

                arr = tmp;
                if(idx > -1){
                    arr.splice(idx, 0, info);
                }else{
                    arr = arr.concat([info]);
                }
            }
        }

        this.deletePlaceholder();

        if(this.props.id === npId || this.props.id === opId){
            this.updateTaskItemOrder((arr.length > 0) ? arr : tmp);
            this.setState({taskList: ((arr.length > 0) ? arr : tmp)});
            this.updateTaskListItem(); //TODO componentDidUpdate
        }
    }

    updateTaskItemOrder = (arr: ITask[]) => {
        arr.forEach((value, i) => value.order = i + 1);
    }

    /**
     * db update order
     */
    updateTaskListItem = () => {
        const arr = this.state.taskList;
        if(arr.length === 0){
            return;
        }

        let updateInfo = [];
        for(const item of arr){
            updateInfo.push({
                "updateOne": {
                    "filter": {"_id": item._id},
                    "update" : {$set: {"order": item.order}}
                }
            });
        }

        Fetch(`/board/tasklist/${this.props.id}/taskitem`, {method: 'PUT', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({opt: updateInfo})})
            .then(() => {
                //console.log(`update taskitem of this tasklist(${this.props.id}) OK`);
            })
            .catch(err => console.log('update taskitem error', err));
    }

    deletePlaceholder = () => {
        this.draggingInfo = null;
        try{
            this.containerRef.current?.removeChild(placeholder);
        }catch(e){ }
    }

    openPopover = (e: MouseEvent) => {
        this.setState({anchorEl: e.currentTarget});
    }

    closePopover = () => {
        this.setState({anchorEl: null});
    }

    clickPopoverMenu = (menuIdx: number) => {
        if(menuIdx === 1){ //add Task
            this.closePopover();

            this.setState({addTask: !this.state.addTask}, () => {
                this.containerRef.current!.insertBefore(this.addTaskContainerRef.current!, this.containerRef.current!.firstChild);
                setTimeout(() => {
                    this.addTaskTextAreaRef.current?.focus();
                });
            });
        }else if(menuIdx === 2){ //delete tasklist
            let result = true;
            if(this.state.taskList.length > 0) {
                result = window.confirm('해당 리스트에 속한 태스크 아이템도 함께 삭제됩니다. 삭제하시겠습니까?');
                if(!result){
                    this.closePopover();
                    return;
                }
            }           

            if(this.state.taskList.length > 0){

                alert('todo');

            }else{
                Fetch(`/board/tasklist/${this.props.id}`, {method: 'DELETE', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({_id: this.props.id})})
                .then(() => {
                    //this.props.notify(this.props.id);
                    this.props.deleteTaskList?.(this.props.id);
                    this.closePopover();
                }).catch(err => console.log('modify TaskContainer Name error', err));
            }
        }
    }

    clickAddTask = () => {
        this.setState({addTask: !this.state.addTask}, () => {
            this.addTaskTextAreaRef.current?.focus();
        });
    }

    //textarea enter key prevent
    keyProcess = (e: KeyboardEvent<HTMLTextAreaElement>) => {
        const code = (e.keyCode ? e.keyCode : e.which);
        if (code === 13) {
            e.preventDefault();
        }
    }

    clickTaskListName = () => {
        this.setState({nameEditing: !this.state.nameEditing});
    }
    
    render(){
        //console.log('------------ render', this.props.id, this.props.name);
        return (
            <div className='task-container'>
                <div className='header'>
                    <textarea className={this.state.nameEditing ? 'editing' : ''} 
                                onBlur={this.modifyTaskContainerName} 
                                onKeyPress={this.keyProcess}
                                onClick={this.clickTaskListName}
                                defaultValue={this.props.name}></textarea>
                    <div className='more' onClick={this.openPopover}><span></span></div>
                    <Popover id={Boolean(this.state.anchorEl) ? 'simple-popover' : undefined}
                            open={Boolean(this.state.anchorEl)}
                            anchorEl={this.state.anchorEl}
                            onClose={this.closePopover}
                            anchorOrigin={{vertical: 'center', horizontal: 'right',}}
                            transformOrigin={{vertical: 'center', horizontal: 'left',}}
                    >
                        <div className='popover-default list-action'>
                            <div className='header'>
                                <span className='title'>List Actions</span>
                                <span className='close' onClick={this.closePopover}></span>
                            </div>
                            <div>
                                <ul>
                                    <li onClick={() => this.clickPopoverMenu(1)}>
                                        Add Task...
                                    </li>
                                    <li onClick={() => this.clickPopoverMenu(2)}>
                                        Delete this...
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </Popover>
                </div>
                <div ref={this.containerRef} 
                    className='contents' 
                    onDragOver={this.dragOver}
                    /*onDragLeave={this.dragLeaveProc}*/
                    onDrop={this.dropProc} 
                    id={this.props.id}>
                    {this.state.taskList.map((el) => <Task key={el._id} id={el._id} subject={el.subject} pId={this.props.id}></Task>)}
                    <div ref={this.addTaskContainerRef} className='add-task-container' style={(this.state.addTask) ? {display:' block'} : {display: 'none'}}>
                        <textarea onBlur={this.addTask} ref={this.addTaskTextAreaRef} placeholder='Enter a title for this task...' autoFocus></textarea>
                        <span></span>
                        <span onMouseDown={this.cancelAddTask}></span>
                    </div>
                    {/* placeholder를 insertBefore 하기 위해 add-card-container div가 lastChild로 필요함, 따라서 엘리먼트 자체는 늘 존재하고 display css 로 show/hide 처리 */}
                    {/* {(this.state.addTask === true ? 
                        <div className='add-card-container'>
                            <textarea onBlur={this.blurAdd} ref={this.addTaskTextAreaRef} placeholder='Enter a title for this task...' autoFocus></textarea>
                            <span onMouseDown={this.cancelAddTask}></span>
                        </div> : null)} */}
                </div>
                {!this.state.addTask && 
                    <div className='footer' onDragOver={this.dragOverFooter}  onDrop={this.dropProc} >
                        <div className='add' 
                            onClick={this.clickAddTask}
                            onMouseOver={this.mouseOverFooter}
                            onMouseOut={this.mouseOutFooter}>&#65291; Add a task</div>
                    </div>
                }
            </div>
        );
    }
}

// const mapDispatchToProps = (dispatch: any) => ({
//     deleteTaskList: (value: String) => dispatch(actions.deleteTaskList(value))
// });

// export default connect(null, mapDispatchToProps)(TaskContainer);
export default connect(null, actions)(TaskContainer);