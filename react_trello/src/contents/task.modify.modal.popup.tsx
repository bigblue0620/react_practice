import React from 'react';
import Modal from 'react-modal';
import './task.modify.modal.popup.scss'

Modal.setAppElement('#root');

function TaskModifyModalPopup(props:any)
{
    const {isOpen, onAfterOpen, onRequestClose, offset } = props;

    return (
        <Modal isOpen={isOpen} onAfterOpen={onAfterOpen} onRequestClose={onRequestClose} shouldCloseOnOverlayClick={true} style={{
            overlay: {
              backgroundColor: 'rgba(0, 0, 0, 0.5)'
            },
            content: {
                background: 'transparent',
                border: 'none',
                position: 'absolute',
                overflow: 'none',
                top: `${offset.top}px`,
                left: `${offset.left}px`,
                width: '260px',
                height: '125px',
                padding: 0
            }
        }}>{props.children}
        </Modal>
    );
}

export default TaskModifyModalPopup;