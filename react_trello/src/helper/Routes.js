import Login from '../member/Login.tsx';
import Home from '../contents/Home';
import Dashboard from '../contents/Dashboard';
import Spread from '../contents/Spread';
import Chart from '../contents/Chart';
import Chart2 from '../contents/Chart2';

const routes = [
    {
        path: '/login',
        component: Login
    },
    {
        path: '/dashboard',
        component: Dashboard
    },
    {
        path: '/spread',
        component: Spread
    },
    {
        path: '/chart',
        component: Chart
    },
    {
        path: '/chart2',
        component: Chart2
    },
    {
        path: '/',
        component: Home
    }
];

export default routes;