export interface IInputState {
    value?: string;
    autofocus?: boolean;
}

export interface IInputProps {
    type: string;
    style: string;
    placeholder?: string;
    autofocus?: boolean; //autofocus
    value?: string;
    action?: any;
}

interface IDashboardState {
    showAddContainerForm: boolean;
};

interface ITask {
    _id: string;
    _pid: string;
    subject: string;
    order?: number;
};

interface ITaskContainerState {
    nameEditing: boolean;
    addTask: boolean;
    taskList: ITask [];
    anchorEl: any;
};

interface ITaskContainerProps {
    id: string;
    name: string;
    deleteTaskList?: (arg1: any) => void;
};

interface ITaskProps {
    id: string;
    pId: string;
    subject?: string;
    h?: number | string;
}

export interface IDynamicData {
    [key: string]: any;
}