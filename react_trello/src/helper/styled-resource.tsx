export const IdStyleStr = `
    width: 400px;
    height: 50px;
    border: solid 1px #d9d9d9;
    padding-left: 20px;
    margin-bottom: 10px;
`;

export const PwStyleStr = IdStyleStr + `
    letter-spacing: 12px;
    font-size: 8px;

    &::placeholder {
        letter-spacing: normal;
        font-size: 14px;
    }
`;

export const TaskStyleStr = `
    background-color: #fff;
    border-radius: 3px;
    box-shadow: 0 1px 0 rgba(9,30,66,.25);
    cursor: pointer;
    display: block;
    margin-bottom: 8px;
    max-width: 300px;
    min-height: 20px;
    position: relative;
    text-decoration: none;
    padding: 6px 8px 2px;
    white-space: normal;
`;

export const TaskSubjectStr = `
    clear: both;
    display: block;
    margin: 0 0 4px;
    width: 235px;
    overflow: hidden;
    word-wrap: break-word;
    color: #172b4d;

    &:hover + button {
        visibility: visible;
    }
`;

export const TaskBtnStr = `
    position: absolute;
    top: 0px;
    right: 4px;
    visibility: hidden;
    background: transparent;
    color: rgba(0, 0, 0, 0.5);

    &:hover {
        visibility: visible;
    }
`;