export const EventEmitter = {
    events: {} as any,
    dispatch: function(event:string, data:any){
        if(!this.events[event]) return;
        this.events[event].forEach((callback:any) => callback(data));
    },
    subscribe: function(event:string, callback:any){
        if(!this.events[event]) this.events[event] = [];
        this.events[event].push(callback);
    }
}

export const Fetch = async (url:string, options:any={}) => {
    const response = await fetch(process.env.REACT_APP_API_URL + url, options);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
};