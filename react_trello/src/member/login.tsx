import React, { Component } from 'react';
import StyledInputElement from '../component/input.component';
import { IdStyleStr, PwStyleStr } from '../helper/styled-resource';
import { actions } from '../redux/action/login';
import { connect } from 'react-redux';
import { IReduxState } from '../redux/reducer/index';
import './Login.scss';

class Login extends Component<ReturnType<typeof mapStateToProps> & typeof actions>
{
    doLogin = () => {
        console.log(`----------------- id=${this.props.id}, pw=${this.props.pw}`);
        //this.props.history.push("/dashboard");
    };

    render()
    {
        const { id, pw, inputID, inputPW } = this.props;

        return (
            <div className='container'>
                <StyledInputElement type='text' style={IdStyleStr} placeholder='ID' action={inputID} autofocus={true} value={id} />
                <StyledInputElement type='password' style={PwStyleStr} placeholder='Password' action={inputPW} autofocus={false} value={pw} />
                <button className='btn' onClick={this.doLogin}>Login</button>
            </div>
        );
    }
}

const mapStateToProps = (state: IReduxState) => ({
    id: state.login?.id,
    pw: state.login?.pw
});

// const mapDispatchToProps = (dispatch: any) => ({
//     inputID: (value: string) => dispatch(actions.inputID(value)),
//     inputPW: (value: string) => dispatch(actions.inputPW(value))
// });

//export default connect(mapStateToProps, mapDispatchToProps)(Login);
export default connect(mapStateToProps, actions)(Login);