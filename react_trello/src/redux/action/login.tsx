import { createAction } from "../common";

// export const LOGIN_INPUT_ID: string = 'INPUT_ID';
// export const LOGIN_INPUT_PW: string = 'INPUT_PW';

export enum ActionType {
    ID ='ID',
    PW = 'PW'
}

// export const inputID = (value: String) => {
//     return {
//         type: LOGIN_INPUT_ID,
//         value
//     }
// };

// export const inputPW = (value: String) => {
//     return {
//         type: LOGIN_INPUT_PW,
//         value
//     }
// };

// export const inputID = (value: string) => createAction(LOGIN_INPUT_ID, value);
// export const inputPW = (value: string) => createAction(LOGIN_INPUT_PW, value);

export const actions = {
    inputID: (value: string) => createAction(ActionType.ID, value),
    inputPW: (value: string) => createAction(ActionType.PW, value)
}