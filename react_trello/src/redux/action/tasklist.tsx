export const TASK_LIST_INIT = 'TASK_LIST_INIT';
export const TASK_LIST_GET = 'TASK_LIST_GET';
export const TASK_LIST_DELETE = 'TASK_LIST_DELETE';
export const TASK_LIST_ADD = 'TASK_LIST_ADD';

export const initTaskList = (value: any) => {
    return {
        type: TASK_LIST_INIT,
        value
    }
};

export const deleteTaskList = (value: String) => {
    return {
        type: TASK_LIST_DELETE,
        value
    }
};

export const addTaskList = (value: any) => {
    return {
        type: TASK_LIST_ADD,
        value
    }
}