import produce from 'immer';

interface IActionType<T extends string> {
    type: T;
}
interface IAction<T extends string, P> extends IActionType<T> {
    value?: P;
}

export function createAction<T extends string>(type: T): IActionType<T>;
export function createAction<T extends string, P>(type: T, value: P): IAction<T, P>;
export function createAction(type: string, value: any = null){
    return typeof value != 'undefined' ? {type, value}: {type};
}

// export function createReducer(initState: any, map: any){
//     return ((state=initState, action: any) => {
//         return produce(state, (draft: any) => {
//             const handler = map[action.type];
//             if(handler){
//                 handler(draft, action);
//             }
//         });
//     });
// }

//A {type: string, value: string}

//Extract<A, IActionType<key>>  ---> type을 추출                                    //[key in Extract<A, 'type'>]
export function createReducer<S, A extends IActionType<string>>(initState: S, map: {[key in A['type']]: (arg1: S, arg2: Extract<A, IActionType<key>>) => void}){
    return ((state:S =initState, action: A) => {
        return produce(state, (draft: S) => {
            //@ts-ignore
            const handler = map[action.type];
            if(handler){
                handler(draft, action);
            }
        });
    });
}
