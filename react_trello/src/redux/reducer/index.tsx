import { combineReducers } from 'redux';
import login, { ILogin } from './login';
import tasklist, { ITaskList } from './tasklist';

export interface IReduxState {
    login?: ILogin;
    tasklist?: ITaskList
}

const rootReducer = combineReducers({
    login,
    tasklist
});

export default rootReducer;