import { ActionType, actions } from '../action/login';
import { createReducer } from '../common';

export interface ILogin {
    id: string;
    pw: string;
};
const INIT_STATE: ILogin = {id: '', pw: ''};

// const reducer = (state=initState, action: any) => 
// {
//     const {type, value} = action;

//     switch(type){
//         case actions.LOGIN_INPUT_ID:
//             return {...state, id: value};
//         case actions.LOGIN_INPUT_PW:
//             return {...state, pw: value};
//         default:
//             return state;
//     }
// };

// const reducer = (state=initState, action: any) => {
//     return produce(state, draft => {
//         switch(action.type){
//             case actions.LOGIN_INPUT_ID:
//                 draft.id = action.value;
//                 break;
//             case actions.LOGIN_INPUT_PW:
//                 draft.pw = action.value;
//                 break;
//             default:
//                 break;
//         }
//     });
// }

// const createReducer = (state: any, action:any, map: any) => {
//     return produce(state, (draft: any) => {
//         if(map[action.type]){
//             return map[action.type](draft, action);
//         }
//     });
// }

// const createReducer = (initState: any, map: any) => {
//     return ((state=initState, action: any) => {
//         return produce(state, (draft: any) => {
//             const handler = map[action.type];
//             if(handler){
//                 handler(draft, action);
//             }
//         })
//     });
// }


// var obj: {[key: string]: any} = {};
// obj[actions.LOGIN_INPUT_ID] = (obj: any, act: any) => {obj.id = act.value};
// obj[actions.LOGIN_INPUT_PW] = (obj: any, act: any) => {obj.pw= act.value};

// { 
//     [actions.LOGIN_INPUT_ID]: ((obj: any, act: any) => {obj.id= act.value}),
//     [actions.LOGIN_INPUT_PW]: ((obj: any, act: any) => {obj.pw= act.value});
// }

//type TAction = keyof typeof actions;  // TAction = "inputID" | "inputPW"
type TAction = ReturnType<typeof actions[keyof typeof actions]> // == ReturnType<typeof actions["inputID" | "inputPW"]
const reducer = createReducer<ILogin, TAction>(INIT_STATE, { 
        [ActionType.ID]: (state, action) => {state.id = action.value || ''},
        [ActionType.PW]: (state, action) => {state.pw = action.value || ''}
});

export default reducer;