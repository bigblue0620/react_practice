import * as actions from '../action/tasklist';

export interface ITaskListItem {
    _id: string;
    name: string;
    enable: boolean | string;
}

export interface ITaskList {
    list: ITaskListItem[];
}

const initState: ITaskList = {list: []};

const reducer = (state=initState, action: any) => 
{
    const {type, value} = action;

    switch(type){
        case actions.TASK_LIST_INIT:
            return {list: [].concat(value)};
        case actions.TASK_LIST_DELETE:
            return {list: state.list.filter((el: any) => el._id !== value)};
        case actions.TASK_LIST_ADD:
            return {list: (state.list as any[]).concat([{...value}])};
        default:
            return state;
    }
};

export default reducer;