import { createStore } from 'redux';

const create = (reducers: any) => {
    return createStore(reducers);
};

export default create;